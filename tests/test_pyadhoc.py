import os

from astropy.io import fits
from unittest import TestCase, skip, main

import pyadhoc
import numpy as np


def test_foo(caplog):
    caplog.set_level(pyadhoc.logging.DEBUG, logger='pyadhoc')
    pass


class TestAdxCreation(TestCase):

    def test_new_tdu_errors(self):

        try:
            os.mkdir('./my_dir')
        except FileExistsError:
            pass

        self.assertRaises(TypeError, pyadhoc.TDU, data='')
        self.assertRaises(ValueError, pyadhoc.TDU, data=np.ones(1))

        self.assertRaises(TypeError, pyadhoc.TDU, trailer='')

        self.assertRaises(TypeError, pyadhoc.TDU, filename=0)
        self.assertRaises(FileNotFoundError, pyadhoc.TDU, filename='foo')
        self.assertRaises(IsADirectoryError, pyadhoc.TDU, filename='./my_dir')

        os.rmdir('./my_dir')


class TestAd2Io(TestCase):

    test_data_path = "tests/data"
    filename_ad2 = os.path.join(test_data_path, 'ad2_data.ad2')
    filename_da2 = os.path.join(test_data_path, 'da2_data.da2')
    filename_fits = os.path.join(test_data_path, 'fits_image.fits')

    def test_ad2_to_ad2(self):

        tdu = pyadhoc.read_ad2(self.filename_ad2)

        output_filename = os.path.join(self.test_data_path, 'temp.ad2')
        pyadhoc.write_ad2(tdu, output_filename, overwrite=True)

        new_tdu = pyadhoc.read_ad2(output_filename)

        np.testing.assert_equal(tdu.trailer.size, new_tdu.trailer.size)
        np.testing.assert_equal(tdu.trailer, new_tdu.trailer)
        np.testing.assert_equal(tdu.data.shape, new_tdu.data.shape)
        np.testing.assert_array_almost_equal(tdu.data, new_tdu.data)

        os.remove(output_filename)

    def test_ad2_vs_fits(self):

        tdu = pyadhoc.read_ad2(self.filename_ad2)
        hdu = fits.open(self.filename_fits)[0]

        np.testing.assert_array_equal(tdu.data, hdu.data)

    def test_ad2_to_fits(self):

        hdu_reference = fits.open(self.filename_fits)[0]

        input_file = self.filename_ad2
        output_file = self.filename_ad2.replace('.ad2', '_ad2.fits')
        pyadhoc.ad2_to_fits(input_file, output_file, overwrite=True)
        hdu = fits.open(output_file)[0]

        np.testing.assert_array_equal(hdu_reference.data.data, hdu.data)

        os.remove(output_file)

    def test_fits_to_ad2(self):

        input_fits = self.filename_fits
        output_ad2 = self.filename_fits.replace('.fits', '_fits.ad2')

        pyadhoc.fits_to_ad2(input_fits, output_ad2, overwrite=True)

        tdu_ref = pyadhoc.read_ad2(self.filename_ad2)
        tdu_new = pyadhoc.read_ad2(output_ad2)

        np.testing.assert_array_equal(tdu_ref.data, tdu_new.data)

        os.remove(output_ad2)

    def test_header_to_ad2_trailer(self):

        header = fits.getheader(self.filename_fits)
        trailer = pyadhoc.fits_header_to_ad2trailer(header)

        tdu = pyadhoc.read_ad2(self.filename_ad2)
        ref_trailer = tdu.trailer

        np.testing.assert_equal(trailer.dtype, ref_trailer.dtype)

        for t, r in zip(trailer, ref_trailer):
            np.testing.assert_array_almost_equal(t, r)

    def test_nparray_to_ad2(self):

        nx = 50
        ny = 60

        xy_grid = np.mgrid[0:ny,0:nx]
        data = np.array(xy_grid[0], dtype=np.float32)

        tdu = pyadhoc.TDU(data=data)

        np.testing.assert_array_equal(data, tdu.data)

        self.assertEqual(data.ndim, tdu.ndim)
        self.assertEqual(data.shape, tdu.shape)

        dt = np.dtype(pyadhoc.TRAILER_AD2)
        self.assertEqual(np.dtype(tdu.trailer.dtype), dt)

        self.assertEqual(data.ndim, tdu.trailer['nbdim'])
        self.assertEqual(data.shape[1], tdu.trailer['lx'])
        self.assertEqual(data.shape[0], tdu.trailer['ly'])
        self.assertEqual(1, tdu.trailer['lz'])

        for key in pyadhoc.DEFAULTS_TRAILER_AD2.keys():

            np.testing.assert_array_equal(
                pyadhoc.DEFAULTS_TRAILER_AD2[key], tdu.trailer[key],
                err_msg='Inconsistent value for parameter {:s}'.format(key)
            )

        tdu.writeto(
            os.path.join(self.test_data_path, 'ndarray.ad2'),
            overwrite=True
        )


class TestAd3Io(TestCase):

    test_data_path = "tests/data"
    filename_ad3 = os.path.join(test_data_path, 'ad3_data.ad3')
    filename_da3 = os.path.join(test_data_path, 'da3_data.da3')
    filename_fits = os.path.join(test_data_path, 'fits_cube.fits')

    def test_ad3_to_ad3(self):

        tdu = pyadhoc.read_ad3(self.filename_ad3)

        output_filename = os.path.join(self.test_data_path, 'temp.ad3')
        pyadhoc.write_ad3(tdu, output_filename, overwrite=True)

        new_tdu = pyadhoc.read_ad3(output_filename)

        np.testing.assert_equal(tdu.trailer.size, new_tdu.trailer.size)
        np.testing.assert_equal(tdu.trailer, new_tdu.trailer)
        np.testing.assert_equal(tdu.data.shape, new_tdu.data.shape)
        np.testing.assert_array_almost_equal(tdu.data, new_tdu.data)

        os.remove(output_filename)

    def test_ad3_vs_fits(self):

        tdu = pyadhoc.read_ad3(self.filename_ad3)
        hdu = fits.open(self.filename_fits)[0]

        np.testing.assert_equal(tdu.data.shape, hdu.data.shape)
        np.testing.assert_equal(tdu.data, hdu.data)

    def test_ad3_to_fits(self):

        hdu_reference = fits.open(self.filename_fits)[0]

        input_file = self.filename_ad3
        output_file = self.filename_ad3.replace('.ad3', '_ad3.fits')
        header_file = self.filename_fits.replace('.fits', '.hdr')

        pyadhoc.ad3_to_fits(input_file, output_file, overwrite=True)

        pyadhoc.ad3_to_fits(input_file, output_file, overwrite=True,
                            header_from=self.filename_fits)

        pyadhoc.ad3_to_fits(input_file, output_file, overwrite=True,
                            header_from=header_file)

        hdu = fits.open(output_file)[0]

        np.testing.assert_equal(hdu_reference.data.shape, hdu.data.shape)
        np.testing.assert_equal(hdu_reference.data, hdu.data)

        os.remove(output_file)

    def test_fits_to_ad3(self):

        input_fits = self.filename_fits
        output_ad3 = self.filename_fits.replace('.fits', '_fits.ad3')

        pyadhoc.fits_to_ad3(input_fits, output_ad3, overwrite=True)

        tdu_ref = pyadhoc.read_ad3(self.filename_ad3)
        tdu_new = pyadhoc.read_ad3(output_ad3)

        np.testing.assert_equal(tdu_ref.data, tdu_new.data)

        os.remove(output_ad3)

    def test_header_to_ad3_trailer(self):

        header = fits.getheader(self.filename_fits)
        trailer = pyadhoc.fits_header_to_ad3trailer(header)

        tdu = pyadhoc.read_ad3(self.filename_ad3)
        ref_trailer = tdu.trailer

        np.testing.assert_equal(trailer.dtype, ref_trailer.dtype)

    def test_nparray_to_ad3(self):

        nx = 50
        ny = 60

        xy_grid = np.mgrid[0:ny,0:nx]
        zyx_grid = np.array(xy_grid)
        data = np.array(zyx_grid, dtype=np.float32)

        tdu = pyadhoc.TDU(data)

        self.assertEqual(data.ndim, tdu.ndim)
        self.assertEqual(data.shape, tdu.shape)

        dt = np.dtype(pyadhoc.TRAILER_AD3)
        self.assertEqual(np.dtype(tdu.trailer.dtype), dt)

        self.assertEqual(data.ndim, tdu.trailer['nbdim'])
        self.assertEqual(data.shape[2], tdu.trailer['lx'])
        self.assertEqual(data.shape[1], tdu.trailer['ly'])
        self.assertEqual(data.shape[0], tdu.trailer['lz'])

        for key in pyadhoc.DEFAULTS_TRAILER_AD3.keys():

            np.testing.assert_array_equal(
                pyadhoc.DEFAULTS_TRAILER_AD3[key], tdu.trailer[key],
                err_msg='Inconsistent value for parameter {:s}'.format(key)
            )

        tdu.writeto(
            os.path.join(self.test_data_path, 'ndarray.ad3'),
            overwrite=True
        )


    def test_read_and_write_ad3_trailer(self):

        filename = os.path.join(self.filename_ad3)

        b = os.path.getsize(filename)
        sz = int((b - 256) / 4)  # size in bytes

        dt = np.dtype([
            ('data', np.float32, sz),
            ('trailer', pyadhoc.TRAILER_AD3)])

        ad3 = np.fromfile(filename, dtype=dt)[0]

        trailer = ad3['trailer']

        ad3_trailer_filename = filename.replace('.ad3', '.adtrailer')
        ad3_trailer = open(ad3_trailer_filename, 'wb')
        ad3_trailer.write(trailer)
        ad3_trailer.close()

        loaded_trailer = np.fromfile(
            ad3_trailer_filename, dtype=pyadhoc.TRAILER_AD3)

        self.assertEqual(loaded_trailer, trailer)

        os.remove(ad3_trailer_filename)

    def test_ad3_read_and_write_and_read_data_without_reshape(self):

        filename = os.path.join(self.filename_ad3)

        b = os.path.getsize(filename)
        sz = int((b - 256) / 4)  # size in bytes

        dt = np.dtype([
            ('data', np.float32, sz),
            ('trailer', pyadhoc.TRAILER_AD3)])

        ad3 = np.fromfile(filename, dtype=dt)[0]

        data = ad3['data']

        ad3_data_filename = filename.replace('.ad3', '.ad3array')
        ad3_data_buffer = open(ad3_data_filename, 'wb')
        ad3_data_buffer.write(data)
        ad3_data_buffer.close()

        loaded_data = np.fromfile(ad3_data_filename, dtype=np.float32)
        np.testing.assert_array_equal(loaded_data, data)

        os.remove(ad3_data_filename)

    def test_ad3_reshape(self):

        filename = os.path.join(self.filename_ad3)

        b = os.path.getsize(filename)
        sz = int((b - 256) / 4)  # size in bytes

        dt = np.dtype([
            ('data', np.float32, sz),
            ('trailer', pyadhoc.TRAILER_AD3)])

        ad3 = np.fromfile(filename, dtype=dt)[0]

        raw_data = ad3['data']

        tdu = pyadhoc.read_ad3(filename)
        reshaped_data = tdu.data
        #reshaped_data = np.flip(reshaped_data, 1)
        reshaped_data = np.transpose(reshaped_data, (1, 2, 0))
        reshaped_data = np.ravel(reshaped_data)

        np.testing.assert_array_equal(raw_data, reshaped_data)

    def test_ad3data_to_ad3data(self):

        filename = os.path.join(self.filename_ad3)
        tdu = pyadhoc.read_ad3(filename)

        data_filename = filename.replace('.ad3', '.ad3data')
        buffer = open(data_filename, 'wb')

        #tdu.data = np.flip(tdu.data, 1)
        tdu.data = np.transpose(tdu.data, (1, 2, 0))
        tdu.data = np.ravel(tdu.data)

        tdu.data[np.isnan(tdu.data)] = -3.1E38
        buffer.write(tdu.data.tobytes())

        original_data = np.fromfile(filename, dtype=np.float32, count=tdu.size)
        rewrite_data = np.fromfile(data_filename, dtype=np.float32)

        np.testing.assert_equal(rewrite_data, original_data)

        os.remove(data_filename)


if __name__ == "__main__":
    main()
