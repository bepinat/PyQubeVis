#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    AdHoc to Fits

    Script created to convert .ad2 and .ad3 files to .fits files. During the
    conversion, the script can get the header from a text file or from another
    fits file.

    by Bruno C. Quint,
    based on code from Benoit Epinat
    Feb 2018
"""

import argparse
import os
import logging
import numpy as np
import sys
import threading

from astropy.io import fits as pyfits

# Trailer structure provided by Benoit Epinat
TRAILER_DATA_TYPE_AD2 = [
    ('nbdim', np.int32),
    ('id1', np.int32),
    ('id2', np.int32),
    ('lx', np.int32),
    ('ly', np.int32),
    ('lz', np.int32),
    ('scale', np.float32),
    ('ix0', np.int32),
    ('iy0', np.int32),
    ('zoom', np.float32),
    ('modevis', np.int32),
    ('thrshld', np.float32),
    ('step', np.float32),
    ('nbiso', np.int32),
    ('pal', np.int32),
    ('cdelt1', np.float64),
    ('cdelt2', np.float64),
    ('crval1', np.float64),
    ('crval2', np.float64),
    ('crpix1', np.float32),
    ('crpix2', np.float32),
    ('crota2', np.float32),
    ('equinox', np.float32),
    ('x_mirror', np.int8),
    ('y_mirror', np.int8),
    ('was_compressed', np.int8),
    ('none2', np.int8, 1),
    ('none', np.int32, 4),
    ('comment', np.int8, 128)
]

# Trailer Structure provided by Philippe Amram.
TRAILER_DATA_TYPE_AD3 = [
    ("nbdim", np.int32),
    ("id1", np.int32),
    ("id2", np.int32),
    ("lx", np.int32),
    ("ly", np.int32),
    ("lz", np.int32),
    ("scale", np.float32),
    ("ix0", np.int32),
    ("iy0", np.int32),
    ("zoom", np.int32),
    ("xl1", np.float32),  # 10 -- lambda of the 1st channel
    ("xil", np.float32),  # 11 -- interfringe in Angstroems
    ("vr0", np.float32),  # 12 -- mean radial velocity of the object
    ("corrvadh", np.float32),  # 13 -- heliocentric RV correction (km/s)
    ("p0", np.float32),  # 14 -- FP interference order
    ("xlp", np.float32),  # 15 -- reference lambda for p0 (in Ansgtroems)
    ("xl0", np.float32),  # 16 -- observed zero-velocity lambda (in Angstroems)
    ("vr1", np.float32),  # 17 -- radial velocity of the 1st channel (in km/s)
    ("xik", np.float32),  # 18 -- interfringe in km/s
    ("cdelt1", np.float64),  # 19 -- unused
    ("cdelt2", np.float64),  # 20 -- unused
    ("crval1", np.float64),  # 21 -- unused
    ("crval2", np.float64),  # 22 -- unused
    ("crpix1", np.float32),  # 23 -- unused
    ("crpix2", np.float32),  # 24 -- unused
    ("crota2", np.float32),  # 25 -- unused
    ("equinox", np.float32),  # 26 -- unused
    ("x_mirror", np.int8),  # 27 -- unused
    ("y_mirror", np.int8),  # 28 -- unused
    ("was_compressed", np.int8),  # 29 -- unused
    ("none2", np.int8),  # 30 -- unused
    ("comment", np.float64)  # 31 -- unused
]

LOG_FORMAT = '  %(message)s'

parser = argparse.ArgumentParser(
    description="Converts .ad2/.ad3 files to .fits.")

parser.add_argument('-d', '--debug', action='store_true',
                    help="Run debug mode.")

parser.add_argument('--header', type=str, default=None,
                    help="ASCII (.hdr or .txt) or FITS (.fits) file that "
                         "contains the header to be copied.")

parser.add_argument('-o', '--output', type=str, default=None,
                    help="Name of the output cube.")

parser.add_argument('-q', '--quiet', action='store_true',
                    help="Run quietly.")

parser.add_argument('input_file', type=str,
                    help="Input .ad2/.ad3 filename.")

parsed_args = parser.parse_args()

log_formatter = logging.Formatter(LOG_FORMAT)

log_handler = logging.StreamHandler()
log_handler.setFormatter(log_formatter)
if parsed_args.debug:
    log_handler.setLevel('DEBUG')
elif not parsed_args.quiet:
    log_handler.setLevel('INFO')
else:
    log_handler.setLevel('WARNING')

log = logging.Logger('ad_to_fits')
log.addHandler(log_handler)

log.info("")
log.info("AdH to Fits Conversion")
log.info("by Bruno Quint (bquint@ctio.noao.edu)")
log.info("Starting program.")
log.info("")

input_file = parsed_args.input_file
log.info("Input File: {input_file:s}".format(**locals()))

output_file = parsed_args.output
if output_file is None:
    if os.path.splitext(input_file)[-1].lower() in ['.ad2']:
        output_file = input_file.replace('.ad2', '.fits')
    if os.path.splitext(input_file)[-1].lower() in ['.ad3']:
        output_file = input_file.replace('.ad3', '.fits')

log.info("Output File: {output_file:s}".format(**locals()))

header_file = parsed_args.header
if header_file is not None:
    log.info("Header File: {header_file:s}".format(**locals()))
else:
    log.info("No header file.")


log.info("Checking input file: {input_file:s}".format(**locals()))

if not os.path.exists(input_file):
    log.error('Input file {input_file:s} does not exists.'
              ' Leaving now.'.format(**locals()))
    sys.exit(1)

if os.path.splitext(input_file)[-1].lower() not in ['.ad2', '.ad3']:
    log.error('Input file {input_file:s} is not .ad2 or .ad3. '
              'Leaving now.'.format(**locals()))
    sys.exit(1)

log.info("Ok.")

log.info("Checking output file: {output_file:s}".format(**locals()))

if os.path.exists(output_file):
    log.error('Output file {output_file:s} already exists. '
              'Delete it and run again.'.format(**locals()))
    sys.exit(1)

log.info("Ok.")


if header_file is not None:
    log.info("Checking header file: {header_file:s}".format(**locals()))

    if not os.path.exists(header_file):
        log.error("Header file {header_file:s} does not exist. "
                  "Leaving now.".format(**locals()))
        sys.exit(1)

    log.info("Ok.")
else:
    log.info("No header file provided.")


log.info("Reading {input_file:s}".format(**locals()))

if os.path.splitext(input_file)[-1].lower() == '.ad2':

    data = open(input_file, 'rb')
    data.seek(0, 2)

    # size of the data array
    sz = int((data.tell() - 256) / 4)
    data.close()

    dt = np.dtype([
        ('data', np.float32, sz),
        ('trailer', TRAILER_DATA_TYPE_AD2)
    ])

    ad2 = np.fromfile(input_file, dtype=dt)[0]

    ndim = ad2['trailer']['nbdim']
    naxis1 = ad2['trailer']['lx']
    naxis2 = ad2['trailer']['ly']
    naxis3 = ad2['trailer']['lz']
    nxyz = naxis1 * naxis2 * naxis3

    data = ad2['data']

    if ndim != 2:
        log.error('{input_file:s} is not a valid .ad2 file - Number of'
                  ' dimensions provided by the trailer is not'
                  ' "2".'.format(**locals()))
        sys.exit(1)

    if naxis1 * naxis2 * naxis3 != data.size:
        log.error('{input_file:s} is not a valid .ad2 file - Dimensions in the'
                  ' trailer and data size mismatch.'.format(**locals()))
        sys.exit(1)

    data = data.reshape(naxis2, naxis1)
    data[np.where(data == -3.1E38)] = np.nan


elif os.path.splitext(input_file)[-1].lower() == '.ad3':

    data = open(input_file, 'rb')
    data.seek(0, 2)

    sz = int((data.tell() - 256) / 4)
    data.close()

    dt = np.dtype([
        ('data', np.float32, sz),
        ('trailer', TRAILER_DATA_TYPE_AD3)
    ])

    ad3 = np.fromfile(input_file, dtype=dt)[0]

    ndim = ad3['trailer']['nbdim']
    naxis1 = ad3['trailer']['lx']
    naxis2 = ad3['trailer']['ly']
    naxis3 = ad3['trailer']['lz']
    nxyz = naxis1 * naxis2 * naxis3

    data = ad3['data']

    if ndim != 3:
        log.error('{input_file:s} is not a valid .ad3 file - Number of '
                  'dimensions provided by the trailer is not '
                  '"3".'.format(**locals()))
        sys.exit(1)

    if naxis1 * naxis2 * naxis3 != data.size:
        log.error('{input_file:s} is not a valid .ad3 file - Dimensions in the '
                  'trailer and data size mismatch.'.format(**locals()))
        sys.exit(1)

    data = np.reshape(data, (naxis2, naxis1, naxis3))
    data = np.transpose(data, (2, 0, 1))
    data = np.flip(data, 1)
    data[np.where(data == -3.1E38)] = np.nan

else:
    log.error('Input file is not an .ad2 or .ad3 file.')
    sys.exit(1)

if header_file is not None:

    log.info("Reading {header_file:s}.".format(**locals()))

    if os.path.splitext(header_file)[-1].lower() in ['.fits']:
        header = pyfits.getheader(header_file)
    elif os.path.splitext(header_file)[-1].lower() in ['.txt', '.hdr']:
        header = pyfits.Header.fromtextfile(header_file)
    else:
        log.error("Invalid header format. Leaving now.")
        sys.exit(1)

else:

    header_file = ''
    header = pyfits.Header()

    if data.ndim == 2:
        header.set('NAXIS', data.ndim)
        header.set('NAXIS1', data.shape[0])
        header.set('NAXIS2', data.shape[1])
    elif data.ndim == 3.:
        header.set('NAXIS', data.ndim)
        header.set('NAXIS1', data.shape[0])
        header.set('NAXIS2', data.shape[1])
        header.set('NAXIS3', data.shape[2])
    else:
        log.error('Data does not have 2 nor 3 dimensions.')
        sys.exit(1)

header.append(card=('AD2FITSH', header_file, "Original Header File."), end=True)
header.append(card=('AD2FITSF', input_file, "Original AD2/AD3 File."), end=True)

log.info("Writing file: {output_file:s}".format(**locals()))
pyfits.writeto(output_file, data, header)
log.info("All done!")
log.info("")
