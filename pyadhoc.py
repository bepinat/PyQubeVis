#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This file is part of PyQubeVis.

PyQubeVis is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

PyQubeVis is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyQubeVis. If not, see <http://www.gnu.org/licenses/>.

Created on 12/2014
@author: Benoît Epinat
"""

import argparse
import logging
import os
import sys

import astropy.io.ascii as astro_ascii
import astropy.io.fits as fits
import numpy as np
from astropy.wcs import WCS
from matplotlib import pyplot as plt

log_formatter = logging.Formatter(' [%(levelname).1s] %(msg)s')
log_handler = logging.StreamHandler()
log_handler.setFormatter(log_formatter)

logger = logging.getLogger('pyadhoc')
logger.setLevel('INFO')
logger.addHandler(log_handler)

logger.debug('pyadhoc - debug enabled')

#
# If matplotlib do not work on your conda virtual environment, try to uninstall
# it via pip and install it via conda with the following commands:
#
# pip uninstall matplotlib
# conda install python.app
# echo 'alias python="pythonw"' >> ~/.bashrc
# conda install matplotlib
#

# verifier l'organisation des cubes fits si on leetos ouvre en python
# faire un programme readad qui voit l'extension pour savoir comment ouvir
# (soit ad2, ad3, ad1...)
# gestion des NaN a inclure (input/output)!!!

# To create an empty ad2 file, I still have to provide the trailer
# as a numpy array and with the proper types.

# --- 04/04/2018 (B. Epinat):
#   we should also add a way to initiate a new ad2 or ad3 TDU from data (image
#   or cube) and put correct information in the trailer. Currently, we can only
#   read files. The 'write_ad?' procedures should enable having no trailer and
#   write a default one. Alternatively, we should be able to create a TDU from
#   data with an associated trailer.We should add tests for ad3 with all options
#   possible (xyz is the usual order for fits files)

TRAILER_AD2 = [
    ('nbdim', np.int32),
    ('id1', np.int32),
    ('id2', np.int32),
    ('lx', np.int32),
    ('ly', np.int32),
    ('lz', np.int32),
    ('scale', np.float32),
    ('ix0', np.int32),
    ('iy0', np.int32),
    ('zoom', np.float32),
    ('modevis', np.int32),
    ('thrshld', np.float32),
    ('step', np.float32),
    ('nbiso', np.int32),
    ('pal', np.int32),
    ('cdelt1', np.float64),
    ('cdelt2', np.float64),
    ('crval1', np.float64),
    ('crval2', np.float64),
    ('crpix1', np.float32),
    ('crpix2', np.float32),
    ('crota2', np.float32),
    ('equinox', np.float32),
    ('x_mirror', np.int8),
    ('y_mirror', np.int8),
    ('was_compressed', np.int8),
    ('none2', np.int8),
    ('none', np.int32, 4),
    ('comment', np.int8, 128)
]

TRAILER_AD3 = [
    ("nbdim", np.int32),  # 0 -- nb of dimension
    ("id1", np.int32),  # 1 -- identification 1/2
    ("id2", np.int32),  # 2 -- identification 2/2
    ("lx", np.int32),  # 3 -- dimension in X
    ("ly", np.int32),  # 4 -- dimension in Y
    ("lz", np.int32),  # 5 -- dimension in Z
    ("scale", np.float32),  # 6 -- arcsec/pixel
    ("ix0", np.int32),  # 7 -- top-left corner
    ("iy0", np.int32),  # 8 -- bottom-right corner
    ("zoom", np.float32),  # 9 -- zoom
    ("xl1", np.float32),  # 10 -- lambda of the 1st channel
    ("xil", np.float32),  # 11 -- interfringe in Angstroms
    ("vr0", np.float32),  # 12 -- mean radial velocity of the object
    ("corrvadh", np.float32),  # 13 -- heliocentric RV correction (km/s)
    ("p0", np.float32),  # 14 -- FP interference order
    ("xlp", np.float32),  # 15 -- reference lambda for p0 (in Ansgtroms)
    ("xl0", np.float32),  # 16 -- observed zero-velocity lambda (in Angstroms)
    ("vr1", np.float32),  # 17 -- radial velocity of the 1st channel (in km/s)
    ("xik", np.float32),  # 18 -- interfringe in km/s
    ("cdelt1", np.float64),  # 19 -- unused
    ("cdelt2", np.float64),  # 20 -- unused
    ("crval1", np.float64),  # 21 -- unused
    ("crval2", np.float64),  # 22 -- unused
    ("crpix1", np.float32),  # 23 -- unused
    ("crpix2", np.float32),  # 24 -- unused
    ("crota2", np.float32),  # 25 -- unused
    ("equinox", np.float32),  # 26 -- unused
    ("x_mirror", np.int8),  # 27 -- unused
    ("y_mirror", np.int8),  # 28 -- unused
    ("was_compressed", np.int8),  # 29 -- unused
    ('none2', np.int8),  # 30 -- unused
    ('comment', np.int8, 128)  # 31 -- unused
]

DEFAULTS_TRAILER_AD2 = {
    'nbdim': 2,
    'id1': 1,
    'id2': 1,
    # 'lx': 0,
    # 'ly': 0,
    # 'lz': 0,
    'scale': 1.0,
    'ix0': 1,
    'iy0': 1,
    'zoom': 1.0,
    'modevis': 1,
    'thrshld': 1.0,
    'step': 1.0,
    'nbiso': 1,
    'pal': 1,
    'cdelt1': 1.0,
    'cdelt2': 1.0,
    'crval1': 1.0,
    'crval2': 1.0,
    'crpix1': 1.0,
    'crpix2': 1.0,
    'crota2': 0.0,
    'equinox': 0.0,
    'x_mirror': 0,
    'y_mirror': 0,
    'was_compressed': 0,
    'none2': 1,
    'none': 0,
    'comment': 1
}

DEFAULTS_TRAILER_AD3 = {
    "nbdim": 3,
    "id1": 1,
    "id2": 1,
    # "lx": 0,
    # "ly": 0,
    # "lz": 0,
    "scale": 1.0,
    "ix0": 1,
    "iy0": 1,
    "zoom": 1.0,
    "xl1": 1.0,
    "xil": 1.0,
    "vr0": 0.0,
    "corrvadh": 0.0,
    "p0": 0.0,
    "xlp": 0.0,
    "xl0": 0.0,
    "vr1": 0.0,
    "xik": 0.0,
    "cdelt1": 1.0,
    "cdelt2": 1.0,
    "crval1": 1.0,
    "crval2": 1.0,
    "crpix1": 1.0,
    "crpix2": 1.0,
    "crota2": 0.0,
    "equinox": 0.0,
    "x_mirror": 1,
    "y_mirror": 1,
    "was_compressed": 0,
    'none2': 1,
    'comment': 1,
}

SAMI_HEADER_KEYS = [
    'OBJECT',
    'OBSTYPE',
    'NOTES',
    'EXPTIME',
    'RA',
    'DEC',
    'TIMESYS',
    'DATE-OBS',
    'TIME-OBS',
    'MJD-OBS',
    'LSTHDR',
    'OBSERVAT',
    'TELESCOP',
    'INSTRUME',
    'TELRADEC',
    'IMAGENUM',
    'TELRA',
    'TELDEC',
    'TELALT',
    'TELAZ',
    'OBSRA',
    'OBSDEC',
    'HA',
    'ZD',
    'AIRMASS',
    'TELFOCUS',
    'ADCNAME',
    'DETECTOR',
    'DETSIZE',
    'NCCDS',
    'NAMPS',
    'PIXSIZE1',
    'PIXSIZE2',
    'PIXSCAL1',
    'PIXSCAL2',
    'RAPANGL',
    'DECPANGL',
    'ROTATOR',
    'ROTOFFS',
    'FILTERS',
    'FILTER1',
    'FILTER2',
    'FILPOS',
    'SHUTSTAT',
    'TV1FOC',
    'ENVTEM',
    'DEWAR',
    'DEWTEM',
    'DEWTEM2',
    'DEWTEM3',
    'CCDTEM',
    'CCDTEM2',
    'WEATDATE',
    'WINDSPD',
    'WINDDIR',
    'AMBTEMP',
    'HUMIDITY',
    'PRESSURE',
    'DIMMSEE',
    'CONTROLR',
    'CONSWV',
    'AMPINTEG',
    'READTIME',
    'OBSERVER',
    'PROPOSER',
    'PROPOSAL',
    'PROPID',
    'OBSID',
    'OPMODE',
    'R0',
    'WSPEED',
    'VARCM',
    'INSTRMT',
    'DMLOOP',
    'FTIME',
    'WFSFLX',
    'WFSFOC',
    'M3LOOP',
    'APDTIME',
    'APDHV',
    'P1LIGHT',
    'P2LIGHT',
    'MNTLOOP',
    'LLTLOOP',
    'RGPOWER',
    'RGDELAY',
    'LGSDIST',
    'LQUALTY',
    'LPOWER',
    'LLTDOOR',
    'GP1X',
    'GP1Y',
    'GP1F',
    'GP2X',
    'GP2Y',
    'GP2F',
    'ADC',
    'ADCTRACK',
    'ADCIN',
    'ADCOUT',
    'VIFOLD',
    'REFLIGHT',
    'REFARM',
    'TSDISK',
    'TSFOLD',
    'TSLIGHT',
    'MODDOOR',
    'FAPERSID',
    'FAPERSWP',
    'FAPERSST',
    'FAPEROTZ',
]


def cd_to_crota(cd):
    '''
    Function that translate CD matrix into old fashioned CDELT + CROTA2
    '''
    crota = {'CDELT1': 0, 'CDELT2': 0, 'CROTA2': 0}
    sign = np.sign(cd['CD1_1'] * cd['CD2_2'] - cd['CD1_2'] * cd['CD2_1'])
    crota['CDELT1'] = sign * np.sqrt(cd['CD1_1'] ** 2 + cd['CD2_1'] ** 2)
    crota['CDELT2'] = np.sqrt(cd['CD1_2'] ** 2 + cd['CD2_2'] ** 2)
    crota['CROTA2'] = np.degrees(np.arctan2(sign * cd['CD1_2'], cd['CD2_2']))
    return crota


def crota_to_cd(crota):
    '''
    Function that translate old fashioned CDELT + CROTA2 into CD matrix
    '''
    cd = {'CD1_1': 0, 'CD1_2': 0, 'CD2_1': 0, 'CD2_2': 0}
    cd['CD1_1'] = crota['CDELT1'] * np.cos(np.radians(crota['CROTA2']))
    cd['CD1_2'] = np.abs(crota['CDELT2']) * np.sign(crota['CDELT1']) * np.sin(np.radians(crota['CROTA2']))
    cd['CD2_1'] = -np.abs(crota['CDELT1']) * np.sign(crota['CDELT2']) * np.sin(np.radians(crota['CROTA2']))
    cd['CD2_2'] = crota['CDELT2'] * np.cos(np.radians(crota['CROTA2']))
    return cd


class TDU:
    """
    Trailer Data Unit

    Class associated to any adhoc format, as HDU (Header Data Unit) for fits
    format.
    """
    
    def __init__(self, data=None, trailer=None, filename=None):
        """
        Parameters
        ----------
        data : None or numpy.ndarray
            Name of the input ada file.

        trailer : None
            The object trailer that holds the metadata.

        filename : None or string
            A string that holds the name of the file used to create the object.
        """
        
        self._check_data(data)
        self._check_trailer(trailer)
        self._check_filename(filename)
        
        if trailer is None:
            if (not data is None):
                trailer = self._trailer_from_data(data)
        
        self.data = data
        self.trailer = trailer
        self.filename = filename
    
    @staticmethod
    def _check_data(data):
        
        if (not data is None):
            
            if (not type(data) is np.ndarray):
                raise TypeError(
                    '"data" is not Numpy Array or None (found {})'.format(
                        type(data)))
            
            if data.ndim not in [2, 3]:
                raise ValueError('invalid number of dimensions for data'
                                 ': {}'.format(data.ndim))
    
    @staticmethod
    def _check_trailer(trailer):
        
        if (not trailer is None):
            
            if type(trailer) not in {np.ndarray, np.void}:
                raise TypeError(
                    '"trailer" is not Numpy Array or None (found {})'.format(
                        type(trailer)))
    
    @staticmethod
    def _check_filename(filename):
        
        if (not filename is None):
            
            if (not type(filename) is str):
                raise TypeError(
                    '"filename" is not String or None (found {})'.format(
                        type(filename)))
            
            if not os.path.exists(filename):
                raise FileNotFoundError('No such file: {}'.format(filename))
            
            if os.path.isdir(filename):
                raise IsADirectoryError('Expected filename, found directory: '
                                        '{}'.format(filename))
    
    @staticmethod
    def _convert_zyx_to_xyz(data):
        return data
    
    @staticmethod
    def _trailer_from_data(data):
        """
        Generate a trailer with default values from a numpy.array.

        Parameters
        ----------
        * data : numpy.array
            A numpy.ndarray with two or three dimensions.

        Returns
        -------
        * trailer :
        """
        
        ndim = np.ndim(data)
        
        if ndim == 2:
            dt = np.dtype(TRAILER_AD2)
            trailer = np.ones((1,), dtype=dt)[0]
            trailer['nbdim'] = data.ndim
            trailer['lx'] = data.shape[1]
            trailer['ly'] = data.shape[0]
            trailer['lz'] = 1
            
            for key in DEFAULTS_TRAILER_AD2.keys():
                trailer[key] = DEFAULTS_TRAILER_AD2[key]
        
        elif ndim == 3:
            dt = np.dtype(TRAILER_AD3)
            trailer = np.ones((1,), dtype=dt)[0]
            trailer['nbdim'] = data.ndim
            trailer['lx'] = data.shape[2]
            trailer['ly'] = data.shape[1]
            trailer['lz'] = data.shape[0]
            
            for key in DEFAULTS_TRAILER_AD3.keys():
                trailer[key] = DEFAULTS_TRAILER_AD3[key]
        
        else:
            raise ValueError('Invalid number of dimensions. Expected 2 or 3. '
                             'Found {}'.format(ndim))
        
        return trailer
    
    def read(self, filename=None):
        raise NotImplemented
    
    def trailertoheader(self, filename=None):
        # return adhoc_trailer_to_fits_header(self)
        raise NotImplemented
    
    def writeto(self, filename=None, overwrite=False):
        """
        Writes current TDU object to a .AD2 or .AD3 file.

        Parameters
        ----------
        * filename : string
            The output filename. If TDU.filename is already defined, this
            parameter overwrites it.

        * overwrite : bool (optional)
            Overwrites output file.

        See Also
        --------
        * write_ad2
        * write_ad3
        """
        
        _filename = None
        if self.filename:
            _filename = self.filename
        
        if filename:
            _filename = filename
        
        if not _filename:
            raise ValueError("'filename' is not defined.")
        
        if self.ndim == 2:
            write_ad2(self, _filename, overwrite=overwrite)
        else:
            write_ad3(self, _filename, overwrite=overwrite)
    
    def writetofits(self, fitsname, third_dim_v=False, xyz=True, overwrite=False):
        hdu = self.tohdu(third_dim_v=third_dim_v, xyz=xyz)
        hdu.writeto(fitsname, overwrite=overwrite)
    
    def convertfits(self, _fits, filename=None):
        raise NotImplemented
    
    def tohdu(self, third_dim_v=False, xyz=True):
        hdu = tdu_to_hdu(self, third_dim_v=third_dim_v, xyz=xyz)  # ajouter option liées aux options de lecture?
        hdul = fits.HDUList([hdu])
        return hdul
        # l'idee est de creer une classe avec des fonctions associees.
        # Au debut, seul le filename est utile
    
    @property
    def size(self):
        return self.data.size
    
    @property
    def shape(self):
        return self.data.shape
    
    @property
    def ndim(self):
        return self.data.ndim


def ad2_trailer():
    raise NotImplemented


def ad3_trailer():
    raise NotImplemented


def read_ad1(filename):
    raise NotImplemented


def read_ad2(filename):
    """Parameters
    ----------
    filename: string
        Name of the input file
    """
    b = os.path.getsize(filename)
    sz = int((b - 256) / 4)  # size in bytes
    
    dt = np.dtype([('data', np.float32, sz), ('trailer', TRAILER_AD2)])
    ad2 = np.fromfile(filename, dtype=dt)[0]
    
    naxis1 = ad2['trailer']['lx']
    naxis2 = ad2['trailer']['ly']
    
    data = ad2['data']
    data = np.reshape(data, (naxis2, naxis1))
    
    data[np.where(data == -3.1E38)] = np.nan
    data = np.ascontiguousarray(data, dtype=np.float32)
    
    trailer = ad2['trailer']
    
    tdu = TDU(data, trailer, filename)
    
    return tdu
    
    # tester l'existence du filename a lire
    # ca serait bien de pouvoir lire les fichiers compresses, je crois que ca
    # existe en python
    
    # info = file_info(realfilename)
    # testgz = strsplit(realfilename, '.', /extract)
    # testgz = testgz[n_elements(testgz)-1]
    
    # if (info.exists eq 0) or (info.read eq 0) or (testgz eq 'gz') then begin
    # ; On regarde si plutot le filename est .ad3.gz...
    # if (testgz ne 'gz') then begin
    # realfilename = filename + '.gz'
    # info = file_info(realfilename)
    # endif else begin
    # realfilename = filename
    # endelse
    
    # if (info.exists eq 0) or (info.read eq 0) then return, -1
    # if (!version.os_family ne 'unix') then return, -1
    
    # testgz = 'gz'
    # spawn, 'gzip -l ' + realfilename, output
    # output = strsplit(output[1], ' ', /extract)
    # size = ulong64(output[1])
    
    # if testgz eq 'gz' then begin
    # trailer.was_compressed = 1
    # endif


def read_ad3(filename, xyz=True):
    """
    Parameters
    ----------
    filename : str
        Name of the input file
    xyz : bool, optional
        False to return data in standard zxy adhoc format
        True  to return data in xyz format (default)
    """
    
    b = os.path.getsize(filename)
    sz = int((b - 256) / 4)  # size in bytes
    # print(b, sz)
    
    dt = np.dtype([('data', np.float32, sz), ('trailer', TRAILER_AD3)])
    ad3 = np.fromfile(filename, dtype=dt)[0]
    
    data = ad3['data']
    trailer = ad3['trailer']
    
    logger.debug("Data Size = {:d}".format(data.size))
    logger.debug("Filename = {:s}".format(filename))
    logger.debug(trailer)
    
    ndim = trailer['nbdim']
    naxis1 = trailer['lx']
    naxis2 = trailer['ly']
    naxis3 = trailer['lz']
    nxyz = naxis1 * naxis2 * naxis3
    
    logger.debug('Number of dimensions = {:d}'.format(ndim))
    
    if np.abs(ndim) != 3:
        raise IOError(
            "{:s} is not a AD3 file. (ndim = {:d})".format(filename, ndim))
    
    if ndim == -3:
        # z, y, x
        data = data.reshape(naxis3, naxis2, naxis1)
    else:
        # y, x, z
        data = data.reshape(naxis2, naxis1, naxis3)
    
    if xyz & (ndim == 3):
        # return the data ordered in z, y, x
        data = data.transpose(2, 0, 1)
    
    if (not xyz) & (ndim == -3):
        # return data ordered in y, x, z
        data = data.transpose(1, 2, 0)
    
    data[np.where(data == -3.1E38)] = np.nan
    data = np.ascontiguousarray(data, dtype=np.float32)
    
    tdu = TDU(data, trailer, filename)
    
    return tdu


def read_adt(filename):
    raise NotImplemented


def read_ada(filename, xsize, ysize):
    """
    Parameters
    ----------
    filename: string
        Name of the input ada file
    xsize: float
        Size of the final image along x-axis
    ysize: float
        Size of the final image along y-axis

    Returns
    -------
    out: ndarray
        Image corresponding to the ada

    """
    
    # Initialization of the image
    im = np.zeros(ysize, xsize)
    # We read the ada file
    ada = np.fromfile(filename, dtype=np.int16)
    # We reshape the data since we know the file is organize with y,x,y,x,y,x...
    data = ada.reshape(ada.size / 2, 2)
    plt.ion()
    image = plt.imshow(im)
    # we loop on each photon to create the image
    for i in range(data.shape[0]):
        # we check the location of each photon is inside the image
        if (data[i][0] < ysize) & (data[i][1] < xsize) & (data[i][0] >= 0) & (
                data[i][1] >= 0):
            im[data[i][0], data[i][1]] += 1
            image.set_data(im)
            # plt.draw()
            # sleep(0.1)
    plt.draw()
    return im
    # it seems that the first frame is duplicated
    # it would be nice to be able to display the creation
    # of the image photon by photon


def read_adz(filename):
    raise NotImplemented


def write_ad1(data, trailer, filename):
    raise NotImplemented


def write_ad2(tdu, filename, overwrite=False):
    """
    Write a TDU to an adhoc format file.

    Parameters
    ----------
    tdu: tdu object
        Data Trailer Unit to be written
    filename: string
        Name of the output file
    overwrite : bool, optional
        If True deletes an existing files before writing it.
    """
    if overwrite is False and os.path.exists(filename):
        raise FileExistsError(
            "File {:s} exits - Delete it first.".format(filename))
    
    else:
        try:
            os.remove(filename)
        except FileNotFoundError:
            pass
    
    ad2 = open(filename, 'wb')
    
    tdu.data[np.isnan(tdu.data)] = -3.1E38
    ad2.write(tdu.data.tobytes())
    
    tdu.data[np.where(tdu.data == -3.1E38)] = np.nan
    ad2.write(tdu.trailer.tobytes())
    ad2.close()
    
    return


def write_ad3(tdu, filename, data_xyz=True, write_xyz=False, overwrite=False):
    """
    Write a TDU to an adhoc format file.

    Parameters
    ----------
    tdu: tdu object
        Data Trailer Unit to be written
    filename: string
        Name of the output file
    data_xyz : bool, optional
        False if data is received in standard zxy adhoc format
        True  if data in received in xyz format (default)
    write_xyz : bool, optional
        False if data has to be written in standard zxy adhoc format (default)
        True  if data has to be written in xyz format
    overwrite : bool, optional
        If True deletes an existing files before writing it.
    """
    
    if overwrite is False and os.path.exists(filename):
        raise FileExistsError(
            "File {:s} exits - Delete it first.".format(filename))
    
    else:
        try:
            os.remove(filename)
        except FileNotFoundError:
            pass
    
    ad3 = open(filename, 'wb')
    
    tdu.data[np.isnan(tdu.data)] = -3.1E38
    
    if data_xyz and (not write_xyz):
        ad3.write(tdu.data.transpose(1, 2, 0).tobytes())
    elif (not data_xyz) and write_xyz:
        ad3.write(tdu.data.transpose(2, 0, 1).tobytes())
    else:
        ad3.write(tdu.data.tobytes())
    tdu.data[np.where(tdu.data == -3.1E38)] = np.nan
    
    if write_xyz:
        tdu.trailer['nbdim'] = -3
    else:
        tdu.trailer['nbdim'] = 3
    
    ad3.write(tdu.trailer.tobytes())
    ad3.close()
    
    return


def ad2_to_fits(ad2_file, fits_file=None, overwrite=False, header_from=None):
    """
    Convert a .ad2 file to a .fits file.

    Parameters
    ----------
        ad2_file (str): The input ad2 filename.
        fits_file (str, optional): The output fits filename.
        overwrite (bool, optional): Overwrite output file if exists.
    """
    if fits_file is None:
        fits_file = os.path.splitext(ad2_file)[0] + '.fits'
    
    logger.info("AD2 to Fits")
    logger.info("Input file: {:s}".format(ad2_file))
    logger.info("Output file: {:s}".format(fits_file))
    
    logger.info("Reading ad2 file")
    tdu = read_ad2(ad2_file)
    logger.info("Ok.")
    
    assert tdu.data.ndim == 2
    
    hdu = fits.PrimaryHDU()
    hdu.data = tdu.data
    
    h = adhoc_trailer_to_fits_header(tdu.trailer)
    
    if header_from is None:
        h.add_blank("")
        # h.add_blank("Empty Header")
        h.add_blank("")
    
    else:
        
        if os.path.splitext(header_from)[-1] == '.fits':
            new_h = fits.open(header_from)[0].header
        
        elif os.path.splitext(header_from)[-1] in ['.hdr', '.txt']:
            new_h = fits.Header.fromtextfile(header_from)
        
        else:
            raise IOError("Wrong file format for header")
        
        if 'OBSERVAT' in new_h.keys() and 'INSTRUME' in new_h.keys():
            logger.info("Copying header from SAMI header.")
            
            for key in SAMI_HEADER_KEYS:
                h[key] = new_h[key]
                h.comments[key] = new_h.comments[key]
            
            h.add_blank('', before=SAMI_HEADER_KEYS[0])
            h.add_blank('Header from SAMI', before=SAMI_HEADER_KEYS[0])
            h.add_blank('', before=SAMI_HEADER_KEYS[0])
            
            wcs = WCS(new_h, fix=True, naxis=2)
            wcs_header = wcs.to_header()
            for key in wcs_header.keys():
                h[key] = wcs_header[key]
            
            h.add_blank('', after=SAMI_HEADER_KEYS[-1])
            h.add_blank('World Coordinate System', after=SAMI_HEADER_KEYS[-1])
            h.add_blank('', after=SAMI_HEADER_KEYS[-1])
    
    hdu.header = h
    
    logger.info("Writing fits file")
    hdu.writeto(fits_file, overwrite=overwrite)
    logger.info("All done.")


def ad3_to_fits(ad3_file, fits_file=None, overwrite=False, third_dim_v=False, xyz=True, header_from=None):
    """
    Convert a .ad3 file to a .fits file.

    Parameters
    ----------
        ad3_file (string): The input ad3 filename.
        fits_file (string): The output fits filename.
        overwrite (bool, optional): Overwrite output file if exists.
        third_dim_v (bool, optional): Indicate whether the third axis of the fits is in velocity (True) or in wavelength (False)
        xyz (bool, optional): Tells if the cube has to be written xyz (True) or zxy (False)
    """
    
    if fits_file is None:
        fits_file = os.path.splitext(ad3_file)[0] + '.fits'
    
    tdu = read_ad3(ad3_file, xyz=xyz)
    
    assert tdu.data.ndim == 3
    
    hdu = tdu_to_hdu(tdu, third_dim_v=third_dim_v, xyz=xyz)
    
    h = hdu.header
    
    if header_from is None:
        h.add_blank("")
        # h.add_blank("Empty Header")
        h.add_blank("")
    
    else:
        if os.path.splitext(header_from)[-1] == '.fits':
            new_h = fits.open(header_from)[0].header
        
        elif os.path.splitext(header_from)[-1] in ['.hdr', '.txt']:
            new_h = fits.Header.fromtextfile(header_from)
        
        else:
            raise IOError("Wrong file format for header")
        
        if 'OBSERVAT' in new_h.keys() and 'INSTRUME' in new_h.keys():
            logger.info("Copying header from SAMI header.")
            
            for key in SAMI_HEADER_KEYS:
                h[key] = new_h[key]
                h.comments[key] = new_h.comments[key]
            
            h.add_blank('', before=SAMI_HEADER_KEYS[0])
            h.add_blank('Header from SAMI', before=SAMI_HEADER_KEYS[0])
            h.add_blank('', before=SAMI_HEADER_KEYS[0])
            
            wcs = WCS(new_h, fix=True, naxis=3)
            wcs_header = wcs.to_header()
            for key in wcs_header.keys():
                h[key] = wcs_header[key]
            
            h.add_blank('', after=SAMI_HEADER_KEYS[-1])
            h.add_blank('World Coordinate System', after=SAMI_HEADER_KEYS[-1])
            h.add_blank('', after=SAMI_HEADER_KEYS[-1])
    
    hdu.header = h
    
    hdu.writeto(fits_file, overwrite=overwrite)


def adhoc_trailer_to_fits_header(trailer, data=None, third_dim_v=False, xyz=True):
    '''
    Function that converts an adhoc trailer to a fits header. Can be used for both 2 and 3 dimension fits headers

    Parameters
    ----------
    trailer: ndarray
        adhoc trailer to be converted
    data: ndarray
        array containing the data to initiate the header. The function should also work if not provided
    third_dim_v: bool
        If true, the information in the header are stored in km/s
    xyz: bool
        Tells if the header has to be created for a xyz (True) or a zxy (False) cube

    Returns
    -------
    Fits header
    '''
    
    if (np.abs(trailer['nbdim']) == 3):
        naxis = 3
        if xyz:
            ax = '1'
            ay = '2'
            az = '3'
            fp_xyz = 1
        else:
            ax = '2'
            ay = '3'
            az = '1'
            fp_xyz = 0
    else:
        naxis = 2
        ax = '1'
        ay = '2'
    
    if data is None:
        h = fits.Header()
        h['SIMPLE'] = (True, "conforms to FITS standard")
        h['BITPIX'] = (-32, "array data type")
        h['NAXIS'] = (naxis, "number of array dimensions")
        h['NAXIS' + ax] = (trailer['lx'], "number of columns (width)")
        h['NAXIS' + ay] = (trailer['ly'], "number of lines (height)")
        if (np.abs(trailer['nbdim']) == 3):
            h['NAXIS' + az] = (trailer['lz'], "number of frames (depth)")
        h['EXTEND'] = True
    else:
        hdu = fits.PrimaryHDU(data)
        h = hdu.header
    
    h['BSCALE'] = 1.0
    h['BZERO'] = 0.0
    
    # This will only work correctly if 3 dimensions cubes are organized as x, y, z
    h['CTYPE' + ax] = ('RA---ARC', 'X-axis type')
    h['CTYPE' + ay] = ('DEC--ARC', 'Y-axis type')
    h['CUNIT' + ax] = ('deg', 'Axis unit')
    h['CUNIT' + ay] = ('deg', 'Axis unit')
    h['EQUINOX'] = (trailer['equinox'], 'Equinox of coordinates')
    
    if (trailer['crval1'] != 0) & (trailer['crval2'] != 0) & (trailer['cdelt1'] != 0) & (trailer['cdelt2'] != 0) & (trailer['crpix1'] != 0) & (trailer['crpix2'] != 0) & (not np.isnan(trailer['crval1'])) & (not np.isnan(trailer['crval2'])) & (not np.isnan(trailer['cdelt1'])) & (not np.isnan(trailer['cdelt2'])) & (not np.isnan(trailer['crpix1'])) & (not np.isnan(trailer['crpix2'])):
        h['CRVAL' + ax] = (trailer['crval1'], 'Reference pixel value')
        h['CRVAL' + ay] = (trailer['crval2'], 'Reference pixel value')
        h['CRPIX' + ax] = (trailer['crpix1'], 'Reference pixel')
        h['CRPIX' + ay] = (trailer['crpix2'], 'Reference pixel')
        h['CROTA2'] = (trailer['crota2'], 'Rotation in degrees')
        h['CDELT' + ax] = (trailer['cdelt1'], 'Degres/pixel')
        h['CDELT' + ay] = (trailer['cdelt2'], 'Degres/pixel')
    else:
        h['CRVAL' + ax] = (DEFAULTS_TRAILER_AD3['crval1'], 'Reference pixel value')
        h['CRVAL' + ay] = (DEFAULTS_TRAILER_AD3['crval2'], 'Reference pixel value')
        h['CRPIX' + ax] = (DEFAULTS_TRAILER_AD3['crpix1'], 'Reference pixel')
        h['CRPIX' + ay] = (DEFAULTS_TRAILER_AD3['crpix2'], 'Reference pixel')
        h['CROTA2'] = (DEFAULTS_TRAILER_AD3['crota2'], 'Rotation in degrees')
        h['CDELT' + ax] = (DEFAULTS_TRAILER_AD3['cdelt1'], 'Degres/pixel')
        h['CDELT' + ay] = (DEFAULTS_TRAILER_AD3['cdelt2'], 'Degres/pixel')
        
    crota = {'CDELT1':h['CDELT' + ax], 'CDELT2':h['CDELT' + ay], 'CROTA2':h['CROTA2']}
    cd = crota_to_cd(crota)
    h['CD{}_{}'.format(ax, ax)] = cd['CD1_1']
    h['CD{}_{}'.format(ax, ay)] = cd['CD1_2']
    h['CD{}_{}'.format(ay, ax)] = cd['CD2_1']
    h['CD{}_{}'.format(ay, ay)] = cd['CD2_2']
    
    if h['NAXIS'] == 3:
        if third_dim_v:
            h['CTYPE' + az] = 'VRAD'
            h['CUNIT' + az] = 'km/s'
            h['CRVAL' + az] = trailer['vr1']
            h['CRPIX' + az] = 1
            h['CDELT' + az] = trailer['xik'] / trailer['lz']
        else:
            h['CTYPE' + az] = 'WAVELENGTH'
            h['CUNIT' + az] = 'Angstrom'
            h['CRVAL' + az] = trailer['xl1']
            h['CRPIX' + az] = 1
            h['CDELT' + az] = trailer['xil'] / trailer['lz']
        
        h['RADESYS'] = 'FK5'
        h['VELREF'] = 3
        h['FP_XYZ'] = (fp_xyz, '0 if fits is in zxy order, 1 if fits is in xyz order')
        h['FP_ID'] = (str(trailer['id1']), 'ADHOC identification')
        h['FP_ORDER'] = (trailer['p0'], 'PF interference order')
        h['FP_L_REF'] = (trailer['xlp'], 'Reference lambda for fp_order (Angstrom)')
        h['FP_B_L'] = (trailer['xl1'], 'Lambda of the first channel (Angstrom)')
        h['FP_B_R'] = (trailer['vr1'], 'Radial velocity of the first channel')
        h['FP_I_A'] = (trailer['xil'], 'Interfringe (Angstrom)')
        h['FP_I_V'] = (trailer['xik'], 'Interfringe (km/s)')
        h['FP_L_RE'] = (trailer['xl0'], 'Lambda rest: observed zero-velocity lambda (Angstrom)')
        h['FP_M_RV'] = (trailer['vr0'], 'Mean RV of the object (km/s)')
        h['FP_H_CO'] = (trailer['corrvadh'], 'Heliocentric correction for the object (km/s)')
    else:
        h['FP_TH'] = (trailer['thrshld'], 'Image display threshold')
        h['FP_STEP'] = (trailer['step'], 'Image display step')
    
    return h


def ad2_trailer_to_fits_header(ad2_trailer):
    raise NotImplemented


def ad3_trailer_to_fits_header(ad3_trailer):
    raise NotImplemented


def tdu_to_hdu(tdu, third_dim_v=False, xyz=True):
    hdu = fits.PrimaryHDU()
    hdu.data = tdu.data
    hdu.header = adhoc_trailer_to_fits_header(tdu.trailer, data=tdu.data, third_dim_v=third_dim_v, xyz=xyz)
    return hdu


def fits_to_ad2(fits_filename, ad2_filename=None, ad2_trailer_name=None,
                overwrite=False):
    """
    Converts a fits file to ad2 file.

    Parameters
    ----------
        fits_filename : str
            name of the input fits file.
        ad2_filename : str
            name of the output ad2 file.
        ad2_trailer_name : string, optional
            name of the file where the header will be stored.
        overwrite : bool, optional
            overwrite the ad2 file.
    """
    
    if ad2_filename is None:
        ad2_filename = os.path.splitext(fits_filename)[0] + '.ad2'
    
    hdulist = fits.open(fits_filename)
    hdu = hdulist[0]
    
    if hdu.header['NAXIS'] != 2:
        raise IOError("Input file as not a valid number or axis.")
    
    tdu = hdu_to_tdu(hdu)
    
    write_ad2(tdu, ad2_filename, overwrite=overwrite)
    
    if (not ad2_trailer_name is None):
        basename, ext = os.path.splitext(ad2_trailer_name)
        
        if (not ext == '.ads'):
            os.path.join(basename, ext, '.ads')
        
        hdu.header.tofile(ad2_trailer_name)


def fits_to_ad3(fits_filename, ad3_filename, ad3_trailer_name=None,
                overwrite=False):
    """
        Converts a fits file to ad2 file.

        Parameters
        ----------
            fits_filename : str
                name of the input fits file.
            ad2_filename : str
                name of the output ad2 file.
            ad2_trailer_name : str, optional
                name of the file where the header will be stored.
            overwrite : bool, optional
                overwrite the ad2 file.
        """
    
    if ad3_filename is None:
        ad3_filename = os.path.splitext(fits_filename)[0] + '.ad3'
    
    hdulist = fits.open(fits_filename)
    hdu = hdulist[0]
    
    if hdu.header['NAXIS'] != 3:
        raise IOError("Input file as not a valid number or axis.")
    
    tdu = hdu_to_tdu(hdu)
    
    write_ad3(tdu, ad3_filename, overwrite=overwrite)
    
    if (not ad3_trailer_name is None):
        basename, ext = os.path.splitext(ad3_trailer_name)
        
        if ext != '.ads':
            os.path.join(basename, ext, '.ads')
        
        hdu.header.tofile(ad3_trailer_name)


def fits_to_adhoc(fits_filename, adhoc_filename, adhoc_trailer=None,
                  overwrite=None):
    """
    Converts a fits file to ad2 or ad3 file depending on the number of
    dimensions.

    Parameters
    ----------
            fits_filename : str
                name of the input fits file.
            adhoc_filename : str
                name of the output ad* file.
            adhoc_trailer : str, optional
                name of the file where the header will be stored.
            overwrite : bool, optional
                overwrite the ad* file.
    """
    hdulist = fits.open(fits_filename)
    hdu = hdulist[0]
    
    if hdu.header['NAXIS'] == 3:
        fits_to_ad3(fits_filename, adhoc_filename, adhoc_trailer, overwrite)
    elif hdu.header['NAXIS'] == 2:
        fits_to_ad2(fits_filename, adhoc_filename, adhoc_trailer, overwrite)
    else:
        logger.error("Invalid input FITS dimensions. Leaving now.")
        sys.exit(1)


def fits_header_to_adhoc_trailer(header):
    '''
    Function that converts a fits header to an adhoc trailer. Can be used for both 2 and 3 dimension fits headers

    Parameters
    ----------
    header: fits_header
        fits trailer to be converted

    Returns
    -------
    Adhoc trailerFits header
    '''
    
    nbdim = header['NAXIS']
    
    if nbdim == 3:
        dt = np.dtype(TRAILER_AD3)
    else:
        dt = np.dtype(TRAILER_AD2)
    
    trailer = np.empty(1, dtype=dt)[0]
    trailer['nbdim'] = nbdim
    
    if trailer['nbdim'] == 3:
        if 'FP_XYZ' in header:
            if header['FP_XYZ'] == 0:
                trailer['lx'] = header['NAXIS2']
                trailer['ly'] = header['NAXIS3']
                trailer['lz'] = header['NAXIS1']
            else:
                trailer['lx'] = header['NAXIS1']
                trailer['ly'] = header['NAXIS2']
                trailer['lz'] = header['NAXIS3']
                trailer['nbdim'] *= -1
        else:
            # We assume the spectral direction is the smallest one, but we could also use the CTYPE information
            idx = np.argmin((header['NAXIS1'], header['NAXIS2'], header['NAXIS3']))
            if idx == 0:
                trailer['lx'] = header['NAXIS2']
                trailer['ly'] = header['NAXIS3']
                trailer['lz'] = header['NAXIS1']
            else:
                trailer['lx'] = header['NAXIS1']
                trailer['ly'] = header['NAXIS2']
                trailer['lz'] = header['NAXIS3']
                trailer['nbdim'] *= -1
        if 'FP_ORDER' in header: trailer['p0'] = header['FP_ORDER']
        if 'FP_L_REF' in header: trailer['xlp'] = header['FP_L_REF']
        if 'FP_L_RE' in header: trailer['xl0'] = header['FP_L_RE']
        if 'FP_M_RV' in header: trailer['vr0'] = header['FP_M_RV']
        if 'FP_H_CO' in header: trailer['corrvadh'] = header['FP_H_CO']
        if 'FP_ID' in header: trailer['id1'] = np.int32(header['FP_ID'])
        
        if 'FP_B_R' in header:
            trailer['vr1'] = header['FP_B_R']
        elif header['CTYPE3'] in ('VELOCITY', 'VRAD'):
            trailer['vr1'] = (header['CDELT3'] * (1 - header['CRPIX3'])) + header['CRVAL3']  # We expect km/s
        if 'FP_I_V' in header:
            trailer['xik'] = header['FP_I_V']
        elif header['CTYPE3'] in ('VELOCITY', 'VRAD'):
            trailer['xik'] = header['CDELT3'] * header['NAXIS3']  # We expect km/s
        
        if 'FP_B_L' in header:
            trailer['xl1'] = header['FP_B_L']
        elif (not header['CTYPE3'] in ('VELOCITY', 'VRAD')):
            trailer['xl1'] = (header['CDELT3'] * (1 - header['CRPIX3'])) + header['CRVAL3']  # We expect Angstroms
        if 'FP_I_A' in header:
            trailer['xil'] = header['FP_I_A']
        elif (not header['CTYPE3'] in ('VELOCITY', 'VRAD')):
            trailer['xil'] = header['CDELT3'] * header['NAXIS3']  # We expect Angstroms
    else:
        trailer['lx'] = header['NAXIS1']
        trailer['ly'] = header['NAXIS2']
        trailer['lz'] = 1
        trailer['modevis'] = 0
        trailer['nbiso'] = 0.
        trailer['pal'] = 0
        trailer['none'] = 0
        # trailer['thrshld'] = 617.3793
        # trailer['step'] = 23.672958
        trailer['thrshld'] = 0.
        trailer['step'] = 0.
        if 'FP_TH' in header: trailer['thrshld'] = header['FP_TH']
        if 'FP_STEP' in header: trailer['step'] = header['FP_STEP']
    
    # Common
    # This will only work correctly if 3 dimensions cubes are organized as x, y, z
    trailer['crval1'] = header['CRVAL1']
    trailer['crval2'] = header['CRVAL2']
    trailer['crpix1'] = header['CRPIX1']
    trailer['crpix2'] = header['CRPIX2']
    
    try:
        trailer['cdelt1'] = header['CDELT1']
        trailer['cdelt2'] = header['CDELT2']
        trailer['crota2'] = header['CROTA2']
    except:
        crota = cd_to_crota(header)
        trailer['cdelt1'] = crota['CDELT1']
        trailer['cdelt2'] = crota['CDELT2']
        trailer['crota2'] = crota['CROTA2']
    
    trailer['scale'] = np.abs(trailer['cdelt1']) * 3600
    
    trailer['equinox'] = header['EQUINOX']
    if trailer['equinox'] == 0:
        trailer['equinox'] = 2000.0
    
    # trailer['id1'] = 0
    trailer['id2'] = 0
    trailer['ix0'] = 0
    trailer['iy0'] = 0
    trailer['zoom'] = 1
    trailer['x_mirror'] = 0.
    trailer['y_mirror'] = 0.
    trailer['was_compressed'] = 0.
    trailer['none2'] = 0
    trailer['comment'] = 0
    
    return trailer


def fits_header_to_ad2_trailer(header):
    # Should use a default header first and then update what is important, same for other metadata transformations.
    dt = np.dtype(TRAILER_AD2)
    trailer = np.empty(1, dtype=dt)[0]
    
    trailer['nbdim'] = header['NAXIS']
    trailer['id1'] = 0
    trailer['id2'] = 0
    trailer['lx'] = header['NAXIS1']
    trailer['ly'] = header['NAXIS2']
    trailer['lz'] = 1
    trailer['scale'] = 0
    trailer['ix0'] = 0
    trailer['iy0'] = 0
    trailer['zoom'] = 0
    trailer['modevis'] = 0
    trailer['thrshld'] = 617.3793
    trailer['step'] = 23.672958
    trailer['nbiso'] = 0.
    trailer['pal'] = 0
    trailer['cdelt1'] = -0.00018826
    trailer['cdelt2'] = 0.00018826
    trailer['crval1'] = 190.93510278
    trailer['crval2'] = 16.39710602
    trailer['crpix1'] = 257
    trailer['crpix2'] = 257
    trailer['crota2'] = 0.7976865
    trailer['equinox'] = 2000
    trailer['x_mirror'] = 0.
    trailer['y_mirror'] = 0.
    trailer['was_compressed'] = 0.
    trailer['none2'] = 0
    trailer['none'] = 0
    trailer['comment'] = 0
    
    return trailer


def fits_header_to_ad3_trailer(header):
    dt = np.dtype(TRAILER_AD3)
    trailer = np.empty(1, dtype=dt)
    
    trailer["nbdim"] = 3
    trailer["id1"] = 0
    trailer["id2"] = 0
    trailer["lx"] = header['naxis1']
    trailer["ly"] = header['naxis2']
    trailer["lz"] = header['naxis3']
    trailer["scale"] = 0
    trailer["ix0"] = 0
    trailer["iy0"] = 0
    trailer["zoom"] = 1
    trailer["xl1"] = 0.
    trailer["xil"] = 0.
    trailer["vr0"] = 0.
    trailer["corrvadh"] = 0.
    trailer["p0"] = 0.
    trailer["xlp"] = 0.
    trailer["xl0"] = 0.
    trailer["vr1"] = 0.
    trailer["xik"] = 0.
    trailer["cdelt1"] = 1.
    trailer["cdelt2"] = 1.
    trailer["crval1"] = 1.
    trailer["crval2"] = 1.
    trailer["crpix1"] = 1.
    trailer["crpix2"] = 1.
    trailer["crota2"] = 1.
    trailer["equinox"] = 1.
    trailer["x_mirror"] = 0
    trailer["y_mirror"] = 0
    trailer["was_compressed"] = 0
    trailer["none2"] = 0
    trailer["comment"] = 0.
    
    # trailer["nbdim"] = header['NAXIS']
    # trailer["id1"] = 0
    # trailer["id2"] = 0
    # trailer["lx"] = header['NAXIS1']
    # trailer["ly"] = header['NAXIS2']
    # trailer["lz"] = header['NAXIS3']
    # trailer["scale"] = 1.
    # trailer["ix0"] = 1.
    # trailer["iy0"] = 1.
    # trailer["zoom"] = 1.
    # trailer["xl1"] = 1.
    # trailer["xil"] = 1.
    # trailer["vr0"] = 1.
    # trailer["corrvadh"] = 1.
    # trailer["p0"] = 1.
    # trailer["xlp"] = 0.
    # trailer["xl0"] = 0.
    # trailer["vr1"] = 0.
    # trailer["xik"] = 0.
    # trailer["cdelt1"] = 1.
    # trailer["cdelt2"] = 1.
    # trailer["crval1"] = 1.
    # trailer["crval2"] = 1.
    # trailer["crpix1"] = 1.
    # trailer["crpix2"] = 1.
    # trailer["crota2"] = 0.
    # trailer["equinox"] = 0
    # trailer["x_mirror"] = 0
    # trailer["y_mirror"] = 0
    # trailer["was_compressed"] = 0
    # trailer["none2"] = 0
    # trailer["comment"] = 0.
    
    return trailer


def hdu_to_tdu(hdu):
    data = hdu.data.astype(np.float32)
    header = hdu.header
    # trailer = fits_header_to_ad2_trailer(header)
    # trailer = fits_header_to_ad3_trailer(header)
    trailer = fits_header_to_adhoc_trailer(header)
    
    tdu = TDU(data, trailer)
    
    return tdu


def fichier_log(filename, header):
    f2 = filename + '_para_ad3.txt'
    f_log = open(f2, 'w')
    f_log.write(
        'nbdim  =   {}  #   0 -- nb of dimension\n'.format(header[0]['nbdim']))
    f_log.write(
        'id1    =   {}  #   1 -- identification 1/2\n'.format(header[0]['id1']))
    f_log.write(
        'id2    =   {}  #   2 -- identification 2/2\n'.format(header[0]['id2']))
    f_log.write(
        'lx     =   {}  #   3 -- dimension in X\n'.format(header[0]['lx']))
    f_log.write(
        'ly     =   {}  #   4 -- dimension in Y\n'.format(header[0]['ly']))
    f_log.write(
        'lz     =   {}  #   5 -- dimension in Z\n'.format(header[0]['lz']))
    f_log.write(
        'scale  =   {}  #   6 -- arcsec/pixel\n'.format(header[0]['scale']))
    f_log.write(
        'ix0    =   {}  #   7 -- top-left corner\n'.format(header[0]['ix0']))
    f_log.write('iy0    =   {}  #   8 -- bottom-right corner\n'.format(
        header[0]['iy0']))
    f_log.write('zoom   =   {}  #   9 -- zoom\n'.format(header[0]['zoom']))
    f_log.write('xl1    =   {}  #   10 -- lambda of the 1st channel\n'.format(
        header[0]['xl1']))
    f_log.write('xil    =   {}  #   11 -- interfringe in Angstroms\n'.format(
        header[0]['xil']))
    f_log.write(
        'vr0    =   {}  #   12 -- mean radial velocity of the object\n'.format(
            header[0]['vr0']))
    f_log.write(
        'corrvadh  =   {}  #   13 -- heliocentric RV correction (km/s)\n'.format(
            header[0]['corrvadh']))
    f_log.write('p0     =   {}  #   14 -- FP interference order\n'.format(
        header[0]['p0']))
    f_log.write(
        'xlp    =   {}  #   15 -- reference lambda for p0 (in Angstroms)\n'.format(
            header[0]['xlp']))
    f_log.write(
        'xl0    =   {}  #   16 -- observed zero-velocity lambda (in Angstroms)\n'.format(
            header[0]['xl0']))
    f_log.write(
        'vr1    =   {}  #   17 -- radial velocity of the 1st channel (in km/s)\n'.format(
            header[0]['vr1']))
    f_log.write('xik    =   {}  #   18 -- interfringe in km/s\n'.format(
        header[0]['xik']))
    f_log.close()
    fichier_log = filename + '_para_ad3.tex'
    astro_ascii.write(header, fichier_log, format='latex', overwrite=True)
    return


def main():
    parser = argparse.ArgumentParser(
        description="Convert AD2/AD3 files to FITS and vice-versa."
    )
    
    parser.add_argument(
        "input",
        help="input filename (fits, ad2 or ad3)."
    )
    
    parser.add_argument(
        "--output",
        default=None,
        help="output filename (fits, ad2 or ad3)."
    )
    
    parser.add_argument(
        "--header",
        default=None,
        help="file that contains a header to be copied."
    )
    
    args = parser.parse_args()
    
    input_file_type = os.path.splitext(args.input)[-1]
    
    if input_file_type.lower() == '.ad2':
        ad2_to_fits(args.input, args.output, header_from=args.header)
    
    elif input_file_type.lower() == '.ad3':
        ad3_to_fits(args.input, args.output, header_from=args.header)
    
    elif input_file_type.lower() == '.fits':
        fits_to_adhoc(args.input, args.output)


if __name__ == "__main__":
    main()
