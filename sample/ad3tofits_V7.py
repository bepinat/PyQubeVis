#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on 2017 November,22
ad3tofits
@author: pamram
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as opt
import array
import astropy.io.fits as fits
import astropy.io.ascii as ascii
import os
from astroquery.ned import Ned
import astropy.units as u
from astropy import constants as const
from ctypes import *


def isl(lamb, pp):
    cel = 299792.458
    isl_ang = lamb / pp * (1 + 1 / (pp * pp))
    isl_vel = cel * isl_ang / lamb
    return (isl_ang, isl_vel)

def fichier_log(fichier,header):
    f2 = fichier + '_para_ad3.txt'
    f_log = open(f2, 'w')
    f_log.write('nbdim  =   {}  #   0 -- nb of dimension\n'.format(header[0]['nbdim']))
    f_log.write('id1    =   {}  #   1 -- identification 1/2\n'.format(header[0]['id1']))
    f_log.write('id2    =   {}  #   2 -- identification 2/2\n'.format(header[0]['id2']))
    f_log.write('lx     =   {}  #   3 -- dimension in X\n'.format(header[0]['lx']))
    f_log.write('ly     =   {}  #   4 -- dimension in Y\n'.format(header[0]['ly']))
    f_log.write('lz     =   {}  #   5 -- dimension in Z\n'.format(header[0]['lz']))
    f_log.write('scale  =   {}  #   6 -- arcsec/pixel\n'.format(header[0]['scale']))
    f_log.write('ix0    =   {}  #   7 -- top-left corner\n'.format(header[0]['ix0']))
    f_log.write('iy0    =   {}  #   8 -- bottom-right corner\n'.format(header[0]['iy0']))
    f_log.write('zoom   =   {}  #   9 -- zoom\n'.format(header[0]['zoom']))
    f_log.write('xl1    =   {}  #   10 -- lambda of the 1st channel\n'.format(header[0]['xl1']))
    f_log.write('xil    =   {}  #   11 -- interfringe in Angstroems\n'.format(header[0]['xil']))
    f_log.write('vr0    =   {}  #   12 -- mean radial velocity of the object\n'.format(header[0]['vr0']))
    f_log.write('corrvadh  =   {}  #   13 -- heliocentric RV correction (km/s)\n'.format(header[0]['corrvadh']))
    f_log.write('p0     =   {}  #   14 -- FP interference order\n'.format(header[0]['p0']))
    f_log.write('xlp    =   {}  #   15 -- reference lambda for p0 (in Ansgtroems)\n'.format(header[0]['xlp']))
    f_log.write('xl0    =   {}  #   16 -- observed zero-velocity lambda (in Angstroems)\n'.format(header[0]['xl0']))
    f_log.write('vr1    =   {}  #   17 -- radial velocity of the 1st channel (in km/s)\n'.format(header[0]['vr1']))
    f_log.write('xik    =   {}  #   18 -- interfringe in km/s\n'.format(header[0]['xik']))
    f_log.close()
    fichier_log = fichier + '_para_ad3.tex'
    ascii.write(header, fichier_log, format='latex', overwrite=True)
    return

def fits_header(header, hdu, dico):
    #     ("nbdim", np.int32),  # 0 -- nb of dimension
    #     ("id1", np.int32),  # 1 -- identification 1/2
    #     ("id2", np.int32),  # 2 -- identification 2/2
    #     ("lx", np.int32),  # 3 -- dimension in X
    #     ("ly", np.int32),  # 4 -- dimension in Y
    #     ("lz", np.int32),  # 5 -- dimension in Z
    #     ("scale", np.float32),  # 6 -- arcsec/pixel
    #     ("ix0", np.int32),  # 7 -- top-left corner
    #     ("iy0", np.int32),  # 8 -- bottom-right corner
    #     ("zoom", np.int32),  # 9 -- zoom
    #     ("xl1", np.float32),  # 10 -- lambda of the 1st channel
    #     ("xil", np.float32),  # 11 -- interfringe in Angstroems
    #     ("vr0", np.float32),  # 12 -- mean radial velocity of the object
    #     ("corrvadh", np.float32),  # 13 -- heliocentric RV correction (km/s)
    #     ("p0", np.float32),  # 14 -- FP interference order
    #     ("xlp", np.float32),  # 15 -- reference lambda for p0 (in Ansgtroems)
    #     ("xl0", np.float32),  # 16 -- observed zero-velocity lambda (in Angstroems)
    #     ("vr1", np.float32),  # 17 -- radial velocity of the 1st channel (in km/s)
    #     ("xik", np.float32),  # 18 -- interfringe in km/s

    naxis1 = header[0]['lx']
    naxis2 = header[0]['ly']
    naxis3 = header[0]['lz']
    SCALEAD = header[0]['scale']
    xl1 = header[0]['xl1']
    xil = header[0]['xil']
    vr0 = header[0]['vr0']
    corrvadh = header[0]['corrvadh']
    p0 = header[0]['p0']
    xlp = header[0]['xlp']
    xl0 = header[0]['xl0']
    vr1 = header[0]['vr1']
    xik = header[0]['xik']
    print('p0=', p0)
    print('vr0=', vr0)
    print('vr1=', vr1)

    OBJECT   = dico['object']
    TELESCOP = dico['telescop']
    INSTRUME = dico['instrume']
    OBSERVER = dico['observer']
    ORIGIN   = dico['origin']
    DATEOBS  = dico['dateobs']
    BUNIT    = dico['bunit']
    EPOCH    = dico['epoch']
    BSCAL    = dico['bscal']
    BSCALE   = dico['bscale']
    BZERO    = dico['bzero']
    PIXEL    = dico['pixel']
    print('BSCALE=',BSCALE,' BZERO=',BZERO,' PIXEL    =',PIXEL)

    #   The first four characters are DEC-, the second four characters contain
    # a four-character code for the projection.
    CTYPE2 = 'DEC---SIN'
    CUNIT2 = 'DEGREE'
    CDELT2 = PIXEL / 3600.
    CRPIX2 = naxis2 / 2. + 1.
    CROTA2 = 0
    FOVX     = PIXEL * naxis1 / 60.
    FOVY     = CDELT2 * naxis2 * 60.

    result_table = Ned.query_object(OBJECT)
    print('OBJECT,result_table.dtype.names =', OBJECT, result_table.dtype.names)
    print('OBJECT,result_table =', OBJECT, result_table)
    CRVAL1 = result_table[0][2]
    CRVAL2 = result_table[0][3]
    print('CRVAL1,CRVAL2=', CRVAL1, CRVAL2)

    # The first four characters are RA--, the second four characters contain
    # a four-character code for the projection.
    # -SIN = orthographic/synthesis, -TAN = gnomonic, ...
    CTYPE1 = 'RA---SIN'
    CUNIT1 = 'DEGREE'
    CDELT1 = -PIXEL / 3600.
    CRPIX1 = naxis1 / 2. + 1
    CROTA1 = 0

    # lha0 = 6562.78
    # lneon = 6598.953
    rv_obj = dico['rv_obj']
    corrv = dico['corrv']
    VELREF   = vr0+corrv
    print('VELREF =', VELREF)

    lneon = dico['wave_eta']
    lha0 = dico['wave_obj']
    WAVE_ETA = lneon
    WAVE_NEB = dico['wave_obj']
    WAVE_SCA = dico['wave_sca']

    p0_ha0  = 609.
    p0_neon = p0_ha0 * lha0 / lneon
    p0_neb  = p0_ha0 * lha0 / WAVE_NEB
    p0_sca  = p0_ha0 * lha0 / WAVE_SCA

    FSR_neon,FSRneon = isl(WAVE_NEB,p0_neon)
    FSR_ha0 ,FSRha0  = isl(lha0, p0_ha0)
    FSR_neb ,FSRneb  = isl(WAVE_NEB,p0_neb)
    FSR_sca ,FSRsca  = isl(WAVE_SCA,p0_sca)

    print('lneon    =', lneon   ,' p0_neon  =', p0_neon,' FSR_neon (A,km/s) =', FSR_neon, FSRneon)
    print('lha0     =', lha0    ,' p0_ha0   =', p0_ha0 ,' FSR_ha0  (A,km/s) =', FSR_ha0, FSRha0)
    print('WAVE_NEB =', WAVE_NEB,' p0_neb   =', p0_neb ,' FSR_neb  (A,km/s) =', FSR_neb, FSRneb)
    print('WAVE_SCA =', WAVE_SCA,' p0_sca   =', p0_sca ,' FSR_sca  (A,km/s) =', FSR_sca, FSRsca)

    P_XBASE = -305.5297951582868
    P_XBASE = 0
    P_STEP  = 12.730408131595283
    P_STEP  = 0
    P_CSTE  = 10.74

    CTYPE3 = 'VELO---HEL'
    CUNIT3 = 'km/s'
    vr_xl1 = 299792.458 * (xl1-lha0) / lha0
    WAVE_VR0 = lha0 * (1+ vr0 / 299792.458)
    FREQ0  = 299792458/xl1/1E-10

    """
    Première itération pour estimer WAVE_CH1
    A ce stade je ne connais pas exactement le FSR donc je ne peux pas calculer exactement WAVE_CH1
    J'estime le FSR la vitesse moyenne de l'objet
    """
    itest  = 100
    p0_vr0 = p0_ha0 * lha0 / WAVE_VR0
    # isl (isl_ang, isl_vel)
    FSR_vr0, FSRvr0 = isl(WAVE_VR0, p0_vr0)
    ivtest = int(VELREF / FSR_vr0) + itest
    for i in range(- ivtest, ivtest):
        if(((vr_xl1 + i * FSRvr0) <= VELREF) & ((vr_xl1 + (i+1) * FSRvr0) >= VELREF)):
            CRVAL3 = vr_xl1 + i * FSRvr0
            WAVE_CH1 = xl1 + i * FSR_vr0
            i_ok = i
    print('i_ok=', i_ok, ' VELREF = ', VELREF, ' vr_xl1 = ', vr_xl1, ' CRVAL3 = ', \
      CRVAL3, ' WAVE_CH1=', WAVE_CH1)

    """
    Seconde itération pour trouver WAVE_CH1 et CRVAL3
    A l'aide de l'estimation de WAVE_CH1, je peux calculer p0_ch1 et FSR_ch1
    Ensuite calcule WAVE_chm la longueur d'onde moyenne au milieu de l'interfrange estimé
    Et enfin le FSR calculé au milieu de l'interfrange
    """
    p0_ch1 = p0_ha0 * lha0 / WAVE_CH1
    FSR_ch1, FSRch1 = isl(WAVE_CH1, p0_ch1)
    WAVE_chm = WAVE_CH1 + FSR_ch1 / 2.
    # isl (isl_ang, isl_vel)
    FSR_ANG, FSR_KMS = isl(WAVE_chm, p0_vr0)
    ivtest = int(VELREF / FSR_KMS) + itest
    for i in range(- ivtest, ivtest):
        if (((vr_xl1 + i * FSR_KMS) <= VELREF) & ((vr_xl1 + (i + 1) * FSR_KMS) >= VELREF)):
            CRVAL3 = vr_xl1 + i * FSR_KMS
            WAVE_CH1 = xl1 + i * FSR_ANG
            i_ok = i
    print('i_ok=', i_ok, ' VELREF = ', VELREF, ' vr_xl1 = ', vr_xl1, ' CRVAL3 = ', \
      CRVAL3, ' WAVE_CH1=', WAVE_CH1)

    WAVE_chm = WAVE_CH1 + FSR_ANG / 2.
    CDELT3 = FSR_KMS / naxis3
    CRVAL3 = CRVAL3 + CDELT3
    print('CRVAL3=',CRVAL3,' CDELT3=',CDELT3,' WAVE_CH1=',WAVE_CH1)
    CRPIX3 = 1.
    CROTA3 = 0.

    BMIN    = 2.
    BMAJ    = 2.
    BPA     = 0.

    hdulist = fits.HDUList([hdu])
    prihdr  = hdulist[0].header

    # BITPIX   = -32
    # prihdr['BITPIX']    = (BITPIX                 , 'bits per data value')
    prihdr['OBJECT']    = (OBJECT                 , 'The observation target')
    prihdr['TELESCOP']  = (TELESCOP               , 'The telescop used')
    prihdr['INSTRUME']  = (INSTRUME               , 'The instrument used')
    prihdr['OBSERVER']  = (OBSERVER               , 'Observer(s) who acquired the data')
    prihdr['ORIGIN']    = (ORIGIN                 , 'Organization responsible for the data')
    prihdr['DATEOBS']   = (DATEOBS                , 'Date of the observations')
    prihdr['BUNIT']     = (BUNIT                  , 'Physical units of the array values')
    prihdr['BSCALE']    = (BSCALE                 , 'Linear factor in scaling equation')
    prihdr['BSCAL']     = (BSCAL                  , 'Linear factor in scaling equation')
    prihdr['BZERO']     = (BZERO                  , 'Zero point in scaling equation')
    prihdr['EPOCH']     = (EPOCH                  , 'Equinox of celestial coordinate system')

    prihdr['SCALEAD']   = (SCALEAD             , 'Pixel size in arcsec from ADHOC')
    prihdr['PIXEL']     = (PIXEL               , 'Pixel size in arcsec')
    prihdr['FOVX']      = (FOVX                 , 'FoV in arcmin (along x-axis)')
    prihdr['FOVY']      = (FOVY                , 'FoV in arcmin (along y-axis)')
    prihdr['CTYPE1']    = (CTYPE1              , 'The type of parameter 1')
    prihdr['CUNIT1']    = (CUNIT1              , 'The unit of parameter 1')
    prihdr['CRVAL1']    = (CRVAL1              , 'Value of RA in px CRPIX1 (in decimal degrees)')
    prihdr['CDELT1']    = (CDELT1              , 'coordinate increment along axis 1 in CUNIT1')
    prihdr['CRPIX1']    = (CRPIX1              , 'coord. syst. ref. px (that has value CRVAL1)')
    prihdr['CROTA1']    = (CROTA1              , 'rotation')

    prihdr['CTYPE2']    = (CTYPE2              , 'The type of parameter 2')
    prihdr['CUNIT2']    = (CUNIT2              , 'The unit of parameter 2')
    prihdr['CRVAL2']    = (CRVAL2              , 'Value of DEC in px CRPIX2 (in decimal degrees)')
    prihdr['CDELT2']    = (CDELT2              , 'coordinate increment along axis 2 in CUNIT2')
    prihdr['CRPIX2']    = (CRPIX2              , 'coord. syst. ref. px (that has value CRVAL2)')
    prihdr['CROTA2']    = (CROTA2              , 'the observation target')

    prihdr['CTYPE3']    = (CTYPE3              , 'The type of parameter 3')
    prihdr['CUNIT3']    = (CUNIT3              , 'The unit of parameter 3')
    prihdr['CRVAL3']    = (CRVAL3              , 'Velocity of channel CRPIX3 in km/s')
    prihdr['CDELT3']    = (CDELT3              , 'coordinate increment along axis 3 in CUNIT3')
    prihdr['CRPIX3']    = (CRPIX3              , 'coord. syst. ref. Channel (having value CRVAL3)')
    prihdr['CROTA3']    = (CROTA3              , 'FoV rotation')
    prihdr['FREQ0 ']    = (FREQ0               , 'Frequency of channel CRPIX3 in Hertz')

    prihdr['p0']        = (p0                     , 'FP interference order')
    prihdr['xlp']       = (xlp                    , 'Reference lambda for p0 (in Ansgtroems)')
    prihdr['xil'] = (xil, 'Interfringe in Angstroems')
    prihdr['xik'] = (xik, 'Interfringe in km/s')
    prihdr['FSR_ANG']   = (FSR_ANG             , 'Free Spectral Range (Angstroems)')
    prihdr['FSR_KMS']   = (FSR_KMS             , 'Free Spectral Range (km/s)')

    prihdr['WAVE_ETA']  = (WAVE_ETA               , 'Wavelength of calibration')
    prihdr['xl0']       = (xl0                    , 'Observed zero-velocity lambda (in Angstroems)')
    prihdr['WAVE_NEB']  = (xl0                    , 'observed zero-velocity lambda (in Angstroems)')
    prihdr['WAVE_SCA']  = (WAVE_SCA               , 'Scanning Wavelength of the object')
    prihdr['xl1']       = (xl1                    , 'Lambda of the 1st channel')
    prihdr['WAVE_CH1']  = (WAVE_CH1               , 'Shifted Lambda of the 1st channel')
    prihdr['WAVE_CHM']  = (WAVE_chm               , 'Shifted Lambda of the medium channel')

    prihdr['vr0']       = (vr0                    , 'mean radial velocity of the object')
    prihdr['WAVE_vr0']  = (WAVE_VR0               , 'Wavelength mean radial velocity of the object')
    prihdr['corrvadh']  = (corrvadh               , 'Heliocentric Radial Velocity Correction')
    prihdr['corrv']     = (corrv                  , 'Heliocentric Radial Velocity Correction')
    prihdr['VELREF']    = (VELREF                 , 'vr0+corrv')
    prihdr['vr1_adh']   = (vr1                    , 'RV of the 1st channel for Adhoc(in km/s)')
    prihdr['vr_xl1']    = (vr_xl1                 , 'RV of the 1st channel (in km/s) i.e. xl1')

    prihdr['P_XBASE']   = (P_XBASE             , 'BCV Stard')
    prihdr['P_STEP']    = (P_STEP              , 'BCV Step')
    prihdr['P_CSTE']    = (P_CSTE              , 'the observation target')

    prihdr['BMAJ  ']    = (BMAJ                , 'Major axis of the beam in arcsec')
    prihdr['BMIN  ']    = (BMIN                , 'Major axis of the beam in arcsec')
    prihdr['BPA   ']    = (BPA                 , 'P.A of beam in degrees anticlockwise direction.')

    return(prihdr,hdulist)

def lecture_ad3(fichier='l2_sky2.ad3'):
    b = os.path.getsize(fichier)
    print('size of file',fichier,'=',b/1e6,' Mb')
    nxyz2 = int((b -256)/4) # size in bytes
    dtype = np.dtype([
        ("nbdim", np.int32),    # 0 -- nb of dimension
        ("id1"  , np.int32),    # 1 -- identification 1/2
        ("id2"  , np.int32),    # 2 -- identification 2/2
        ("lx"   , np.int32),    # 3 -- dimension in X
        ("ly"   , np.int32),    # 4 -- dimension in Y
        ("lz"   , np.int32),    # 5 -- dimension in Z
        ("scale", np.float32),  # 6 -- arcsec/pixel
        ("ix0"  , np.int32),    # 7 -- top-left corner
        ("iy0"  , np.int32),    # 8 -- bottom-right corner
        ("zoom" , np.int32),    # 9 -- zoom
        ("xl1"  , np.float32),  # 10 -- lambda of the 1st channel
        ("xil"  , np.float32),  # 11 -- interfringe in Angstroems
        ("vr0"  , np.float32),  # 12 -- mean radial velocity of the object
        ("corrvadh", np.float32),  # 13 -- heliocentric RV correction (km/s)
        ("p0"   , np.float32),  # 14 -- FP interference order
        ("xlp"  , np.float32),  # 15 -- reference lambda for p0 (in Ansgtroems)
        ("xl0"  , np.float32),  # 16 -- observed zero-velocity lambda (in Angstroems)
        ("vr1"  , np.float32),  # 17 -- radial velocity of the 1st channel (in km/s)
        ("xik"  , np.float32),  # 18 -- interfringe in km/s
        ("cdelt1", np.float64), # 19 -- unused
        ("cdelt2", np.float64), # 20 -- unused
        ("crval1", np.float64), # 21 -- unused
        ("crval2", np.float64), # 22 -- unused
        ("crpix1", np.float32), # 23 -- unused
        ("crpix2", np.float32), # 24 -- unused
        ("crota2", np.float32), # 25 -- unused
        ("equinox", np.float32),# 26 -- unused
        ("x_mirror", np.int8),  # 27 -- unused
        ("y_mirror", np.int8),  # 28 -- unused
        ("was_compressed", np.int8),  # 29 -- unused
        ("none2", np.int8),     # 30 -- unused
        ("comment",np.float64)  # 31 -- unused
    ])
    f = open(fichier, "rb")
    f.seek(nxyz2*4, os.SEEK_SET)
    header = np.fromfile(f, dtype=dtype)
    print('header=',header)
    f.close()
    print(np.shape(header))

    fichier_log(fichier,header)

    naxis1 = header[0]['lx']
    naxis2 = header[0]['ly']
    naxis3 = header[0]['lz']
    print('naxis1=', naxis1, ' range(naxis1-1)=', range(naxis1 - 1))
    print('naxis2=', naxis2, ' range(naxis2-1)=', range(naxis2 - 1))
    print('naxis3=', naxis3, ' range(naxis3-1)=', range(naxis3 - 1))
    nxyz = naxis1 * naxis2 * naxis3
    print('naxis1 * naxis2 * naxis3=', nxyz,' = nxyz2 =', nxyz2)
    print('nxyz*4+256 =', nxyz * 4 + 256,' = size of file=', b)

    f = open(fichier, "rb")
    print('Input file=',fichier,' type(f):',type(f))
    # rb= read binary, 'f' : float, 'h' : integer
    binvalues = array.array('f')  # ad2
    binvalues.fromfile(f, nxyz)
    data_3d = np.array(binvalues, dtype='float32')
    # data_3d = np.reshape(data_3d, (naxis1, naxis2, naxis3))
    data_3d = np.reshape(data_3d, (naxis2, naxis1, naxis3))

    print('LECTURE OF THE AD3 FILE, please wait')

# return the data_3d x, y, z ordered in z, y, x ==> data_3d = data_3d.transpose(2, 0, 1)
# return the data_3d x, y, z ordered in y, x, z ==> data_3d = data_3d.transpose(1, 2, 0)
    data_3d = np.transpose(data_3d,(2,0,1))
    data_3d = np.flip(data_3d,1)

    print('COMPUTATION OF THE AD2 FILE, please wait')

    data_2d = np.sum(data_3d,axis=0)/naxis3

    return(data_3d,data_2d,header)

def main():

    # Open the ad3 file

    # fichier = 'rf0250loh.ad3'
    fichier = '/media/bquint/BTFI_01/SAMFP/SAMFP_Oct2016/20160930/015_rf0250_42cx/loh.ad3'
    #vecteur = ['Messier31','SOAR','SAMFP','CdM,BQ,PhA']
    lha0 = 6562.78
    lneon = 6598.953

    dico = {}
    # dico['object'] = 'Messier31'
    # dico['object'] = 'SDSSJ122040.31+024529.6'
    dico['object'] = 'UM 345'
    dico['telescop'] = 'SOAR'
    dico['instrume'] = 'SAMFP'
    dico['observer'] = 'CdM,BQ,PhA'
    dico['origin'] = 'Cerro Pachon'
    dico['dateobs'] = '20160930'
    dico['bunit'] = 'Photons/s/px'
    dico['epoch'] = 2000.
    dico['bscale']  = 1.
    # dico['bscal']   = 1000.
    dico['bscal']   = 1.
    dico['bzero']   = 0.
    dico['pixel']   = 0.250
    dico['wave_eta']   = lneon
    dico['wave_obj']   = lha0
    dico['wave_sca']   = lneon
    dico['rv_obj']     = 5683
    dico['corrv']  = +8.01

    data_3d, data_2d, header = lecture_ad3(fichier)
    data_3d = data_3d * dico['bscal']
    data_2d = data_2d * dico['bscal']
    naxis1 = header[0]['lx']
    naxis2 = header[0]['ly']
    naxis3 = header[0]['lz']
    print('naxis1,naxis2,naxis3=',naxis1,naxis2,naxis3)
    print('input file:',type(data_3d))
    # print(data_3d)

    min_data_3d       = np.min(data_3d[data_3d > 0])
    max_data_3d       = np.max(data_3d[data_3d > 0])
    moyenne_data_3d   = np.mean(data_3d[data_3d > 0])
    mediane_data_3d   = np.median(data_3d[data_3d > 0])
    sigma_data_3d     = np.std(data_3d[data_3d > 0])
    variance_data_3d  = np.var(data_3d[data_3d > 0])
    print('The minimum  :', min_data_3d)
    print('The maximum  :', max_data_3d)
    print('The mean     :', moyenne_data_3d)
    print('The mediane  :', mediane_data_3d)
    print('Stand. dev.  :', sigma_data_3d)
    print('The variance :', variance_data_3d)


    """
        Enregistrement du fichier .ad3 en format fits
    """
    print('enregistrement du fichier .ad3 en format fits')
    filename_3d = fichier+'.fits'  # nom du fichier de sortie
    print('\n Recording of the fits file',filename_3d,' please wait ...')

    # hdu = fits.PrimaryHDU(data_3d.reshape(naxis3, naxis1, naxis2))
    hdu = fits.PrimaryHDU(data_3d.reshape(naxis3, naxis2, naxis1))
    print('header du main=',header)
    prihdr, hdulist = fits_header(header,hdu,dico)
    print(prihdr)
    hdulist.writeto(filename_3d, overwrite=True)
    # hdulist.writeto(filename_3d, mode='update')

    """
         Enregistrement du fichier somme du cube .ad2 en format fits
    """
    print('enregistrement du fichier .ad2 en format fits')
    filename_2d = fichier + '.sum.fits'  # nom du fichier de sortie
    print('\n Recording of the fits file', filename_2d, ' please wait ...')

    # hdu = fits.PrimaryHDU(data_3d.reshape(naxis3, naxis1, naxis2))
    hdu = fits.PrimaryHDU(data_2d.reshape(naxis2, naxis1))
    print('header du main=', header)
    prihdr, hdulist = fits_header(header, hdu, dico)
    print(prihdr)
    hdulist.writeto(filename_2d, overwrite=True)
    # hdulist.writeto(filename_3d, mode='update')

    print("\n ... The data cube and the sum of the data image are written in fits format ...")

    """
        Plot the sum of the data cube (read above)
    """

    nxyz = naxis1 * naxis2 * naxis3
    x = np.linspace(0, naxis1 - 1, naxis1)
    y = np.linspace(0, naxis2 - 1, naxis2)
    x, y = np.meshgrid(x, y)

    moyenne_data_2d   = np.mean(data_2d[data_2d > 0])
    mediane_data_2d   = np.median(data_2d[data_2d > 0])
    sigma_data_2d     = np.std(data_2d[data_2d > 0])
    print('The mean     :', moyenne_data_2d)
    print('The mediane  :', mediane_data_2d)
    print('Stand. dev.  :', sigma_data_2d)

    fig, ax = plt.subplots(1, 1)
    cax = ax.imshow(data_2d.reshape(naxis2, naxis1), cmap=plt.cm.jet, origin='bottom',
            vmin=moyenne_data_2d -  1 * sigma_data_2d,
            vmax=moyenne_data_2d +  1 * sigma_data_2d,
            extent=(x.min(), x.max(), y.min(), y.max()),
            interpolation = 'nearest')

    # cax = ax.imshow(data_2d.reshape(naxis2, naxis1), cmap=plt.cm.jet)
    # ax.contour(x, y, data_2d.reshape(naxis2, naxis1), 8, colors='w')
    ax.tick_params(axis='both', which='both', direction='in', top='on', right='on')
    ax.set_title(fichier)
    # Add colorbar, make sure to specify tick locations to match desired ticklabels
    # cbar = fig.colorbar(cax, ticks=[-1,0,1])
    cbar = fig.colorbar(cax)
    # cbar.ax.set_yticklabels([500,1000,1500])  # vertically oriented colorbar


    plt.show()

if __name__ == "__main__":
    main()
