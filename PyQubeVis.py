#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This file is part of PyQubeVis.

PyQubeVis is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

PyQubeVis is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyQubeVis.  If not, see <http://www.gnu.org/licenses/>.

Created on 12/2014
@author: Benoît Epinat
"""

import sys
import os
import platform
import astropy.io.fits as fits
#import pyfits as pf
import numpy as np
import gzip
#import pywcs
#import ipdb
import argparse

from PyQt5 import QtGui, QtCore, QtWidgets
from PyQt5.QtWidgets import QApplication, QMainWindow, QTextEdit, QPushButton, QMessageBox
#from PyQt5.QtGui import QApplication, QMainWindow, QTextEdit, QPushButton, QMessageBox
from PyQt5.QtCore import Qt
from PyQt5.QtCore import QT_VERSION_STR
from PyQt5.Qt import PYQT_VERSION_STR

import signal
signal.signal(signal.SIGINT, signal.SIG_DFL)

#import matplotlib
#matplotlib.use('Qt5Agg')  # Qt5Agg backend
#from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
#from matplotlib.figure import Figure

import pylab as pl
import pyadhoc as pad

from ui_pyqubevis_qt5 import Ui_PyQubeVis

__version__ = '0.1'

#class mplCanvas(FigureCanvas):
    #def __init__(self):
        #self.fig = Figure()
        #self.ax = self.fig.add_subplot(111)
        #self.fig.subplots_adjust(left=0., bottom=0., right=1., top=1.)
        #self.ax.axes.tick_params(bottom=False, top=False, left=False, right=False)
        ##for spine in self.ax.spines.itervalues(): #python 2.7
            ##spine.set_visible(False)
        #for spine in self.ax.spines.values():
            #spine.set_visible(False)
    
        #FigureCanvas.__init__(self, self.fig)
        #FigureCanvas.setSizePolicy(self, QtWidgets.QSizePolicy.Expanding,QtWidgets.QSizePolicy.Expanding)
        #FigureCanvas.updateGeometry(self)


#class matplotlibWidget(QtWidgets.QWidget):
    #def __init__(self, parent = None):
        #QtWidgets.QWidget.__init__(self, parent)
        #self.canvas = mplCanvas()
        #self.vbl = QtWidgets.QVBoxLayout()
        #self.vbl.addWidget(self.canvas)
        #self.setLayout(self.vbl)


class MainWindow(QMainWindow, Ui_PyQubeVis):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        self.setupUi(self)
        self.initUI()

        self.actionOpen.triggered.connect(self.openfile)
        self.actionSave.triggered.connect(self.savefile)
        #self.actionSaveps.triggered.connect(self.saveps)
        #self.actionClose.triggered.connect(self.closeim)
        self.actionCoordinates.triggered.connect(self.coordinatesDock)
        self.actionProfile.triggered.connect(self.profileDock)
        self.actionDisplay.triggered.connect(self.displayDock)
        self.coordinatesDockWidget.visibilityChanged.connect(self.coordinatesDockClosed)
        self.profileDockWidget.visibilityChanged.connect(self.profileDockClosed)
        self.displayDockWidget.visibilityChanged.connect(self.displayDockClosed)
        self.actionReset.triggered.connect(self.initUI)
        self.actionQuit.triggered.connect(QtCore.QCoreApplication.instance().quit)
        self.actionAbout.triggered.connect(self.about)
        self.graphicsView.onMouseMoved.connect(self.mouseMoved)
        self.graphicsView.onMousePressed.connect(self.mousePressed)
        self.graphicsView.onMouseReleased.connect(self.mouseReleased)
        self.graphicsView.onWheel.connect(self.wheel)
        self.flipxCheckBox.stateChanged.connect(self.flipxAction)
        self.flipyCheckBox.stateChanged.connect(self.flipyAction)
        self.zoominButton.clicked.connect(self.zoominAction)
        self.zoomoutButton.clicked.connect(self.zoomoutAction)
        self.lowThresholdLineEdit.textChanged.connect(self.updateLowThresholds)
        self.highThresholdLineEdit.textChanged.connect(self.updateHighThresholds)
        self.colorTableInvertCheckBox.stateChanged.connect(self.updateInvertCT)
        self.colorTableComboBox.currentIndexChanged.connect(self.updateColorTable)
        self.scaleComboBox.currentIndexChanged.connect(self.updateScale)
        self.zoomImageComboBox.currentIndexChanged.connect(self.updateZoomSize)
        self.channelSlider.valueChanged.connect(self.updateProfile)
        self.channelLineEdit.textChanged.connect(self.updateProfile2)
        self.sumCubeCheckBox.stateChanged.connect(self.updateProfileSum)
        self.lowMinusButton.clicked.connect(self.updateLowMinusThreshold)
        self.highMinusButton.clicked.connect(self.updateHighMinusThreshold)
        self.lowPlusButton.clicked.connect(self.updateLowPlusThreshold)
        self.highPlusButton.clicked.connect(self.updateHighPlusThreshold)
        self.thresholdComboBox.editTextChanged.connect(self.updateThresholdStep)

    def updateProfileSum(self):
        if self.argfiles['sz'][self.numim - 1] == 0:
            return
        if self.sumCubeCheckBox.isChecked():
            self.highThresholdLineEdit.setText(self.argfiles['formatval'][self.numim - 1]%(self.argfiles['sumhighthr'][self.numim - 1]))
            self.lowThresholdLineEdit.setText(self.argfiles['formatval'][self.numim - 1]%(self.argfiles['sumlowthr'][self.numim - 1]))
            self.argfiles['issum'][self.numim - 1] = True
            self.channelSlider.setEnabled(False)
            self.channelLineEdit.setEnabled(False)
            self.im = self.argfiles['sum'][self.numim - 1]
        else:
            self.highThresholdLineEdit.setText(self.argfiles['formatval'][self.numim - 1]%(self.argfiles['highthr'][self.numim - 1]))
            self.lowThresholdLineEdit.setText(self.argfiles['formatval'][self.numim - 1]%(self.argfiles['lowthr'][self.numim - 1]))
            self.argfiles['issum'][self.numim - 1] = False
            self.channelSlider.setEnabled(True)
            self.channelLineEdit.setEnabled(True)
            index = self.argfiles['profilenumber'][self.numim - 1]
            self.im = self.argfiles['data'][self.numim - 1][index - 1, :, :]
            self.channelSlider.setMaximum(self.argfiles['sz'][self.numim - 1])
            self.channelSlider.setMinimum(1)
            self.channelSlider.setValue(index)
        self.updateLowThresholds()
        self.updateHighThresholds()
        self.updateDisplay()

    def updateProfile(self):
        index = self.channelSlider.value()
        self.argfiles['profilenumber'][self.numim - 1] = index
        self.channelLineEdit.setText(str(index))
        self.im = self.argfiles['data'][self.numim - 1][index - 1, :, :]
        self.updateDisplay()

    def updateProfile2(self):
        index = int(self.channelLineEdit.text())
        self.argfiles['profilenumber'][self.numim - 1] = index
        self.channelSlider.setValue(index)
        self.im = self.argfiles['data'][self.numim - 1][index - 1, :, :]
        self.updateDisplay()

    def displayDockClosed(self):
        if not self.displayDockWidget.isVisible():
            self.actionDisplay.setChecked(False)
        else:
            self.actionDisplay.setChecked(True)

    def coordinatesDockClosed(self):
        if not self.coordinatesDockWidget.isVisible():
            self.actionCoordinates.setChecked(False)
        else:
            self.actionCoordinates.setChecked(True)

    def profileDockClosed(self):
        if not self.profileDockWidget.isVisible():
            self.actionProfile.setChecked(False)
        else:
            self.actionProfile.setChecked(True)

    def displayDock(self):
        if self.actionDisplay.isChecked():
            self.displayDockWidget.setVisible(True)
        else:
            self.displayDockWidget.setVisible(False)

    def profileDock(self):
        if self.actionProfile.isChecked():
            self.profileDockWidget.setVisible(True)
        else:
            self.profileDockWidget.setVisible(False)

    def coordinatesDock(self):
        if self.actionCoordinates.isChecked():
            self.coordinatesDockWidget.setVisible(True)
        else:
            self.coordinatesDockWidget.setVisible(False)

    def createColorTable(self, cmap):
        """
        """
        #inds=np.linspace(0,255,256)
        #sm=pl.cm.ScalarMappable(cmap=cmap)
        #rgbas = sm.to_rgba(inds)
        #return [QtGui.QColor(int(np.round(r * 255)), int(np.round(g * 255)),int(np.round(b * 255)), int(np.round(a * 255))).rgba() for r, g, b, a in rgbas]

        # 255 colors + white for last element
        inds = np.concatenate((np.linspace(0, 255, 255), [0]))
        sm = pl.cm.ScalarMappable(cmap=cmap)
        rgbas = sm.to_rgba(inds)
        rgbas[255] = [1, 1, 1, 1]
        return [QtGui.QColor(int(np.round(r * 255)), int(np.round(g * 255)), int(np.round(b * 255)), int(np.round(a * 255))).rgba() for r, g, b, a in rgbas]

    def updateColorTable(self):
        self.argfiles['colortable'][self.numim - 1] = self.createColorTable(str(self.colorTableComboBox.currentText()))

        if self.colorTableInvertCheckBox.isChecked():
            self.ctImage = QtGui.QImage(np.uint8(254 - np.arange(255).reshape(1, 255)), 255, 1, 255, QtGui.QImage.Format_Indexed8)
        else:
            self.ctImage = QtGui.QImage(np.uint8(np.arange(255).reshape(1, 255)), 255, 1, 255, QtGui.QImage.Format_Indexed8)

        self.ctImage.setColorTable(self.argfiles['colortable'][self.numim - 1])
        self.ctPixmap = QtGui.QPixmap.fromImage(self.ctImage)
        self.colorTableLabel.setPixmap(self.ctPixmap)
        self.updateDisplay()

    def updateZoomPixmap(self):
        if self.sumCubeCheckBox.isChecked():
            q = np.where(np.isnan(self.im))
            ql = np.where(np.nan_to_num(self.im) <= self.argfiles['sumlowthr'][self.numim - 1])
            qh = np.where(np.nan_to_num(self.im) >= self.argfiles['sumhighthr'][self.numim - 1])
            a = (np.float32(self.im) - self.argfiles['sumlowthr'][self.numim - 1]) * 254 / np.float32(self.argfiles['sumhighthr'][self.numim - 1] - self.argfiles['sumlowthr'][self.numim - 1])
        else:
            q = np.where(np.isnan(self.im))
            ql = np.where(np.nan_to_num(self.im) <= self.argfiles['lowthr'][self.numim - 1])
            qh = np.where(np.nan_to_num(self.im) >= self.argfiles['highthr'][self.numim - 1])
            a = (np.float32(self.im) - self.argfiles['lowthr'][self.numim - 1]) * 254 / np.float32(self.argfiles['highthr'][self.numim - 1] - self.argfiles['lowthr'][self.numim - 1])
        #q=np.where(np.isnan(self.argfiles['data'][self.numim - 1] <= self.argfiles['lowthr'][self.numim - 1]))
        #ql=np.where(self.argfiles['data'][self.numim - 1] <= self.argfiles['lowthr'][self.numim - 1])
        #qh=np.where(self.argfiles['data'][self.numim - 1] >= self.argfiles['highthr'][self.numim - 1])
        #a=(np.float32(self.argfiles['data'][self.numim - 1])-self.argfiles['lowthr'][self.numim - 1])*254/np.float32(self.argfiles['highthr'][self.numim - 1]-self.argfiles['lowthr'][self.numim - 1])
        a[ql] = 0
        a[qh] = 254
        a[q] = 255
        minth = 0.
        maxth = 254.

        if self.argfiles['ctscale'][self.numim - 1] == 'Log':
            a = np.log10(a + 1)
            minth = np.log10(minth + 1)
            maxth = np.log10(maxth + 1)
        if self.argfiles['ctscale'][self.numim - 1] == 'Sqrt':
            a = np.sqrt(a)
            minth = np.sqrt(minth)
            maxth = np.sqrt(maxth)
        if self.argfiles['ctscale'][self.numim - 1] == 'Squared':
            a = np.power(a, 2)
            minth = np.power(minth, 2)
            maxth = np.power(maxth, 2)
        if self.argfiles['ctscale'][self.numim - 1] == 'Exp':
            a = np.exp(a / 256.)
            minth = np.exp(minth / 256.)
            maxth = np.exp(maxth / 256.)

        a = np.uint8((a - minth) * 254 / (maxth - minth))
        if self.colorTableInvertCheckBox.isChecked():
            a = 254 - a
        a[q] = 255

        a2 = np.zeros((a.shape[0] + self.zoomSize - 1, a.shape[1] + self.zoomSize - 1), dtype=np.uint8) + 255
        a2[np.uint8((self.zoomSize - 1) / 2.):np.uint8((self.zoomSize - 1) / 2.) + a.shape[0], np.uint8((self.zoomSize - 1) / 2.):np.uint8((self.zoomSize - 1) / 2.) + a.shape[1]] = a

        #QImage creation
        image = QtGui.QImage(a2, self.argfiles['sx'][self.numim - 1] + self.zoomSize - 1, self.argfiles['sy'][self.numim - 1] + self.zoomSize - 1, self.argfiles['sx'][self.numim - 1] + self.zoomSize - 1, QtGui.QImage.Format_Indexed8)
        image.setColorTable(self.argfiles['colortable'][self.numim - 1])
        self.argfiles['zoompixmap'][self.numim - 1] = (QtGui.QPixmap.fromImage(image))
        self.zoomImItem.setPixmap(self.argfiles['zoompixmap'][self.numim - 1].copy(0, 0, self.zoomSize, self.zoomSize))
        
        try:
            self.zoomImItem.setPixmap(self.argfiles['zoompixmap'][self.numim - 1].copy(np.int32(self.graphicsView.mapToScene(self.graphicsView.pos).x()),np.int32(self.graphicsView.mapToScene(self.graphicsView.pos).y()), self.zoomSize, self.zoomSize ))
        except:
            return

    def updateDisplay(self):
        if self.sumCubeCheckBox.isChecked():
            q = np.where(np.isnan(self.im))
            ql = np.where(np.nan_to_num(self.im) <= self.argfiles['sumlowthr'][self.numim - 1])
            qh = np.where(np.nan_to_num(self.im) >= self.argfiles['sumhighthr'][self.numim - 1])
            a = (np.float32(self.im) - self.argfiles['sumlowthr'][self.numim - 1]) * 254 / np.float32(self.argfiles['sumhighthr'][self.numim - 1] - self.argfiles['sumlowthr'][self.numim - 1])
        else:
            q = np.where(np.isnan(self.im))
            ql = np.where(np.nan_to_num(self.im) <= self.argfiles['lowthr'][self.numim - 1])
            qh = np.where(np.nan_to_num(self.im) >= self.argfiles['highthr'][self.numim - 1])
            a = (np.float32(self.im) - self.argfiles['lowthr'][self.numim - 1]) * 254 / np.float32(self.argfiles['highthr'][self.numim - 1] - self.argfiles['lowthr'][self.numim - 1])
        #q=np.where(np.isnan(self.argfiles['data'][self.numim - 1] <= self.argfiles['lowthr'][self.numim - 1]))
        #ql=np.where(self.argfiles['data'][self.numim - 1] <= self.argfiles['lowthr'][self.numim - 1])
        #qh=np.where(self.argfiles['data'][self.numim - 1] >= self.argfiles['highthr'][self.numim - 1])
        #a=(np.float32(self.argfiles['data'][self.numim - 1])-self.argfiles['lowthr'][self.numim - 1])*254/np.float32(self.argfiles['highthr'][self.numim - 1]-self.argfiles['lowthr'][self.numim - 1])
        a[ql] = 0
        a[qh] = 254
        a[q] = 255
        minth = 0.
        maxth = 254.

        if self.argfiles['ctscale'][self.numim - 1] == 'Log':
            a = np.log10(a + 1)
            minth = np.log10(minth + 1)
            maxth = np.log10(maxth + 1)
        if self.argfiles['ctscale'][self.numim - 1] == 'Sqrt':
            a = np.sqrt(a)
            minth = np.sqrt(minth)
            maxth = np.sqrt(maxth)
        if self.argfiles['ctscale'][self.numim - 1] == 'Squared':
            a = np.power(a, 2)
            minth = np.power(minth, 2)
            maxth = np.power(maxth, 2)
        if self.argfiles['ctscale'][self.numim - 1] == 'Exp':
            a = np.exp(a / 256.)
            minth = np.exp(minth / 256.)
            maxth = np.exp(maxth / 256.)

        a = np.uint8((a - minth) * 254 / (maxth - minth))
        if self.colorTableInvertCheckBox.isChecked():
            a = 254 - a
        a[q] = 255

        #QImage creation
        image = QtGui.QImage(a, self.argfiles['sx'][self.numim - 1], self.argfiles['sy'][self.numim - 1], self.argfiles['sx'][self.numim - 1], QtGui.QImage.Format_Indexed8)
        image.setColorTable(self.argfiles['colortable'][self.numim - 1])
        self.argfiles['pixmap'][self.numim - 1] = (QtGui.QPixmap.fromImage(image))

        self.imItem.setPixmap(self.argfiles['pixmap'][self.numim - 1])
        self.scene.setSceneRect(QtCore.QRectF(self.argfiles['pixmap'][self.numim - 1].rect()))

        self.updateZoomPixmap()

    def updateScale(self):
        self.argfiles['ctscale'][self.numim - 1] = str(self.scaleComboBox.currentText())
        self.updateDisplay()

    def updateThresholdStep(self):
        self.argfiles['thresholdstep'][self.numim - 1] = self.thresholdComboBox.currentText()

    def updateLowMinusThreshold(self):
        if self.sumCubeCheckBox.isChecked():
            self.argfiles['sumlowthr'][self.numim - 1] = np.float32(str(self.lowThresholdLineEdit.text())) - np.float32(str(self.thresholdComboBox.currentText()))
            self.lowThresholdLineEdit.setText(self.argfiles['formatval'][self.numim - 1]%(self.argfiles['sumlowthr'][self.numim - 1]))
        else:
            self.argfiles['lowthr'][self.numim - 1] = np.float32(str(self.lowThresholdLineEdit.text())) - np.float32(str(self.thresholdComboBox.currentText()))
            self.lowThresholdLineEdit.setText(self.argfiles['formatval'][self.numim - 1]%(self.argfiles['lowthr'][self.numim - 1]))
        self.updateLowThresholds()

    def updateLowPlusThreshold(self):
        if self.sumCubeCheckBox.isChecked():
            self.argfiles['sumlowthr'][self.numim - 1] = np.float32(str(self.lowThresholdLineEdit.text())) + np.float32(str(self.thresholdComboBox.currentText()))
            self.lowThresholdLineEdit.setText(str(self.argfiles['sumlowthr'][self.numim - 1]))
        else:
            self.argfiles['lowthr'][self.numim - 1] = np.float32(str(self.lowThresholdLineEdit.text())) + np.float32(str(self.thresholdComboBox.currentText()))
            self.lowThresholdLineEdit.setText(str(self.argfiles['lowthr'][self.numim - 1]))
        self.updateLowThresholds()

    def updateHighMinusThreshold(self):
        if self.sumCubeCheckBox.isChecked():
            self.argfiles['sumhighthr'][self.numim - 1] = np.float32(str(self.highThresholdLineEdit.text())) - np.float32(str(self.thresholdComboBox.currentText()))
            self.highThresholdLineEdit.setText(self.argfiles['formatval'][self.numim - 1]%(self.argfiles['sumhighthr'][self.numim - 1]))
        else:
            self.argfiles['highthr'][self.numim - 1] = np.float32(str(self.highThresholdLineEdit.text())) - np.float32(str(self.thresholdComboBox.currentText()))
            self.highThresholdLineEdit.setText(self.argfiles['formatval'][self.numim - 1]%(self.argfiles['highthr'][self.numim - 1]))
        self.updateHighThresholds()

    def updateHighPlusThreshold(self):
        if self.sumCubeCheckBox.isChecked():
            self.argfiles['sumhighthr'][self.numim - 1] = np.float32(str(self.highThresholdLineEdit.text())) + np.float32(str(self.thresholdComboBox.currentText()))
            self.highThresholdLineEdit.setText(self.argfiles['formatval'][self.numim - 1]%(self.argfiles['sumhighthr'][self.numim - 1]))
        else:
            self.argfiles['highthr'][self.numim - 1] = np.float32(str(self.highThresholdLineEdit.text())) + np.float32(str(self.thresholdComboBox.currentText()))
            self.highThresholdLineEdit.setText(self.argfiles['formatval'][self.numim - 1]%(self.argfiles['highthr'][self.numim - 1]))
        self.updateHighThresholds()

    def updateLowThresholds(self):
        if self.sumCubeCheckBox.isChecked():
            try:
                self.argfiles['sumlowthr'][self.numim - 1] = np.float32(str(self.lowThresholdLineEdit.text()))
            except:
                self.argfiles['sumlowthr'][self.numim - 1] = np.float32(0)
            try:
                self.argfiles['sumlowthr'][self.numim - 1] = np.float32(str(self.lowThresholdLineEdit.text()))
            except:
                self.argfiles['sumlowthr'][self.numim - 1] = np.float32(0)
        else:
            
            try:
                self.argfiles['lowthr'][self.numim - 1] = np.float32(str(self.lowThresholdLineEdit.text()) )
            except:
                self.argfiles['lowthr'][self.numim - 1] = np.float32(0)
        self.updateDisplay()

    def updateHighThresholds(self):
        if self.sumCubeCheckBox.isChecked():
            self.argfiles['sumhighthr'][self.numim - 1] = np.float32(str(self.highThresholdLineEdit.text()))
        else:
            self.argfiles['highthr'][self.numim - 1] = np.float32(str(self.highThresholdLineEdit.text()))
        self.updateDisplay()

    def updateInvertCT(self):
        self.argfiles['ctinvert'][self.numim - 1] = self.colorTableInvertCheckBox.isChecked()
        self.updateColorTable()

    def flipxAction(self):
        if self.numim == 0:
            return
        if self.argfiles['flipx'][self.numim - 1] != self.flipxCheckBox.isChecked():
            self.graphicsView.setTransform(QtGui.QTransform(np.float32(-1.), np.float32(0.), np.float32(0.), np.float32(1.), np.float32(0.), np.float32(0.)), combine=True)
            self.zoomGraphicsView.setTransform(QtGui.QTransform(np.float32(-1.), np.float32(0.), np.float32(0.), np.float32(1.), np.float32(0.), np.float32(0.)), combine=True)

        self.argfiles['flipx'][self.numim - 1] = self.flipxCheckBox.isChecked()

    def flipyAction(self):
        if self.numim == 0:
            return
        if self.argfiles['flipy'][self.numim - 1] != self.flipyCheckBox.isChecked():
            self.graphicsView.setTransform(QtGui.QTransform(np.float32(1.), np.float32(0.), np.float32(0.), np.float32(-1.), np.float32(0.), np.float32(0.)), combine=True)
            self.zoomGraphicsView.setTransform(QtGui.QTransform(np.float32(1.), np.float32(0.), np.float32(0.), np.float32(-1.), np.float32(0.), np.float32(0.)), combine=True)
        self.argfiles['flipy'][self.numim - 1] = self.flipyCheckBox.isChecked()

    def zoominAction(self):
        if self.numim == 0:
            return
        fac = self.argfiles['zoom'][self.numim - 1] * 2.
        self.argfiles['zoom'][self.numim - 1] = fac
        self.graphicsView.scale(2, 2)

    def zoomoutAction(self):
        if self.numim == 0:
            return
        fac = self.argfiles['zoom'][self.numim - 1] / 2.
        self.argfiles['zoom'][self.numim - 1] = fac
        self.graphicsView.scale(0.5, 0.5)

    def updateZoomSize(self):
        self.zoomSize = (self.zoomImageComboBox.currentIndex()) * 2 + 11

        self.zoomCentralPixelFrame.setGeometry(int(self.zoomGraphicsView.x() + (float(self.zoomGraphicsView.width()) / self.zoomSize) * (self.zoomSize - 1) / 2.),
                                               int(self.zoomGraphicsView.y() + (float(self.zoomGraphicsView.height()) / self.zoomSize) * (self.zoomSize - 1) / 2.),
                                               int((float(self.zoomGraphicsView.width()) / self.zoomSize) + 1),
                                               int((float(self.zoomGraphicsView.height()) / self.zoomSize) + 1))
        self.zoomImItem = self.zoomGraphicsScene.addPixmap(QtGui.QPixmap())
        self.zoomGraphicsScene.setSceneRect(QtCore.QRectF(0, 0, self.zoomSize, self.zoomSize))
        self.zoomGraphicsView.setTransform(QtGui.QTransform(np.float32(1.), np.float32(0.), np.float32(0.), np.float32(1.), np.float32(0.), np.float32(0.)), combine=False)
        self.zoomGraphicsView.scale(float(self.zoomGraphicsView.width()) / self.zoomSize, float(self.zoomGraphicsView.height()) / self.zoomSize)
        if self.numim != 0:
            self.updateZoomPixmap()

    def mousePressed(self):
        if self.numim == 0:
            return
        if self.argfiles['numcube'][self.numim - 1] != 0:
            return
        self.xpress = np.int32(np.floor(self.graphicsView.mapToScene(self.graphicsView.pos).x()))
        self.ypress = np.int32(np.floor(self.graphicsView.mapToScene(self.graphicsView.pos).y()))
        if self.graphicsView.pshift | self.graphicsView.pctrl:
            if self.xpress < 0:
                self.xpress = 0
            if self.ypress < 0:
                self.ypress = 0
            if self.xpress >= self.argfiles['sx'][self.numim - 1]:
                self.xpress = self.argfiles['sx'][self.numim - 1] - 1
            if self.ypress >= self.argfiles['sy'][self.numim - 1]:
                self.ypress = self.argfiles['sy'][self.numim - 1] - 1
        else:
            #if (self.xpress < 0) | (self.xpress >= self.argfiles['sx'][self.numim - 1]) | (self.ypress < 0) | (self.ypress >= self.argfiles['sy'][self.numim - 1]):
            #self.argfiles['data'][self.numim - 1][self.ypress, self.xpress] = np.nan
            #self.im = self.argfiles['data'][self.numim - 1]
            #self.updateDisplay()
            return

    def mouseReleased(self):
        if self.numim == 0:
            return
        if self.argfiles['numcube'][self.numim - 1] != 0:
            return
        self.xrelease = np.int32(np.floor(self.graphicsView.mapToScene(self.graphicsView.pos).x()))
        self.yrelease = np.int32(np.floor(self.graphicsView.mapToScene(self.graphicsView.pos).y()))
        if self.graphicsView.rshift & self.graphicsView.pshift:
            if self.xrelease < 0:
                self.xrelease = 0
            if self.yrelease < 0:
                self.yrelease = 0
            if self.xrelease >= self.argfiles['sx'][self.numim - 1]:
                self.xrelease = self.argfiles['sx'][self.numim - 1] - 1
            if self.yrelease >= self.argfiles['sy'][self.numim - 1]:
                self.yrelease = self.argfiles['sy'][self.numim - 1] - 1
            if (self.ypress <= self.yrelease) & (self.xpress <= self.xrelease):
                self.argfiles['data'][self.numim - 1][self.ypress:self.yrelease + 1, self.xpress:self.xrelease + 1] = np.nan
            elif (self.ypress <= self.yrelease) & (self.xpress > self.xrelease):
                self.argfiles['data'][self.numim - 1][self.ypress:self.yrelease + 1, self.xrelease:self.xpress + 1] = np.nan
            elif (self.ypress > self.yrelease) & (self.xpress <= self.xrelease):
                self.argfiles['data'][self.numim - 1][self.yrelease:self.ypress + 1, self.xpress:self.xrelease + 1] = np.nan
            else:
                self.argfiles['data'][self.numim - 1][self.yrelease:self.ypress + 1, self.xrelease:self.xpress + 1] = np.nan
        elif self.graphicsView.rctrl & self.graphicsView.pctrl:
            if (self.ypress <= self.yrelease):
                self.argfiles['data'][self.numim - 1][:self.ypress,:] = np.nan
                self.argfiles['data'][self.numim - 1][self.yrelease + 1:,:] = np.nan
            else:
                self.argfiles['data'][self.numim - 1][:self.yrelease,:] = np.nan
                self.argfiles['data'][self.numim - 1][self.ypress + 1:,:] = np.nan                
            if (self.xpress <= self.xrelease):
                self.argfiles['data'][self.numim - 1][:,:self.xpress] = np.nan
                self.argfiles['data'][self.numim - 1][:,self.xrelease + 1:] = np.nan
            else:
                self.argfiles['data'][self.numim - 1][:,:self.xrelease] = np.nan
                self.argfiles['data'][self.numim - 1][:,self.xpress + 1:] = np.nan                
        else:
            if (self.xrelease < 0) | (self.xrelease >= self.argfiles['sx'][self.numim - 1]) | (self.yrelease < 0) | (self.yrelease >= self.argfiles['sy'][self.numim - 1]):
                return
            self.argfiles['data'][self.numim - 1][self.yrelease, self.xrelease] = np.nan
        self.im = self.argfiles['data'][self.numim - 1]
        self.updateDisplay()
        
    def mouseMoved(self):
        if self.numim == 0:
            return
        if (self.graphicsView.mapToScene(self.graphicsView.pos).x() < 0) | (self.graphicsView.mapToScene(self.graphicsView.pos).x() >= self.argfiles['sx'][self.numim - 1]) | (self.graphicsView.mapToScene(self.graphicsView.pos).y() < 0) | (self.graphicsView.mapToScene(self.graphicsView.pos).y() >= self.argfiles['sy'][self.numim - 1]):
            return

        x = self.graphicsView.mapToScene(self.graphicsView.pos).x()
        y = self.graphicsView.mapToScene(self.graphicsView.pos).y()

        self.xposLabel.setText('X = %.1f'%x)
        self.yposLabel.setText('Y = %.1f'%y)
        self.valLabel.setText('Val = ' + self.argfiles['formatval'][self.numim - 1]%(self.im[np.int32(y), np.int32(x)]))
        #gerer l'affichage des spectres si un profil est chargé
        self.zoomImItem.setPixmap(self.argfiles['zoompixmap'][self.numim - 1].copy(np.int32(x), np.int32(y), self.zoomSize, self.zoomSize))

        if self.numcube != 0:
            minip = np.nanmin(self.argfiles['data'][self.numcube - 1][:, np.int32(min(y, self.argfiles['sy'][self.numcube - 1])), np.int32(min(x, self.argfiles['sx'][self.numcube - 1]))])
            maxip = np.nanmax(self.argfiles['data'][self.numcube - 1][:, np.int32(min(y, self.argfiles['sy'][self.numcube - 1])), np.int32(min(x, self.argfiles['sx'][self.numcube - 1]))])
            self.minProfileLabel.setText('Min = ' + self.argfiles['formatval'][self.numim - 1]%(minip))
            self.maxProfileLabel.setText('Max = ' + self.argfiles['formatval'][self.numim - 1]%(maxip))
            
            #XXX ICI
            #XXX Option pour ne pas afficher de profil même si un cube a été chargé (ou alors pas les bonnes dimensions)
            # Do not update the spectrum if we reach an all-nan-slice or if the cube depth is only 1
            #print(self.argfiles['data'][self.numcube - 1][:, np.int32(min(y, self.argfiles['sy'][self.numcube - 1])), np.int32(min(x, self.argfiles['sx'][self.numcube - 1]))])
            if not np.isnan(minip) and not np.isnan(maxip) and minip != maxip:
                self.line.set_ydata(self.argfiles['data'][self.numcube - 1][:, np.int32(min(y, self.argfiles['sy'][self.numcube - 1])), np.int32(min(x, self.argfiles['sx'][self.numcube - 1]))])
                self.profileWidgetDisplay.canvas.ax.axis([0, self.argfiles['sz'][self.numcube-1], minip, maxip])
                self.profileWidgetDisplay.canvas.ax.draw_artist(self.profileWidgetDisplay.canvas.ax.patch)
                self.profileWidgetDisplay.canvas.ax.draw_artist(self.line)
                self.profileWidgetDisplay.canvas.update()
                self.profileWidgetDisplay.canvas.flush_events()

    def wheel(self):
        if self.graphicsView.delta.y() < 0:
            self.zoomoutAction()
        elif self.graphicsView.delta.y() > 0:
            self.zoominAction()
    
    def initUI(self):
        """
        """
        #Building Tips (menus, buttons, etc.)
        #l'ideal serait les docstrings des sous-fonctions/slots
        QtWidgets.QToolTip.setFont(QtGui.QFont('SansSerif', 10))
        #self.setToolTip('This is the main window')
        self.setTabPosition(Qt.AllDockWidgetAreas, QtWidgets.QTabWidget.North)
        #self.tabifyDockWidget(self.coordinatesDockWidget, self.profileDockWidget)
        #self.tabifyDockWidget(self.profileDockWidget, self.displayDockWidget)

        #self.profileWidgetDisplay.canvas.blit(self.profileWidgetDisplay.canvas.ax.bbox)

        self.numim = 0
        self.numcube = 0
        self.numberofcubes = 0

        #Color Table list
        self.defaultColorTable = 'nipy_spectral'
        maps = [m for m in pl.cm.datad if not m.endswith("_r")]
        maps.sort()
        for m in maps:
            self.colorTableComboBox.addItem(m)
        #self.colorTableComboBox.setCurrentIndex(self.colorTableComboBox.findText(QtCore.QString(self.defaultColorTable))) #python2.7
        self.colorTableComboBox.setCurrentIndex(self.colorTableComboBox.findText(str(self.defaultColorTable)))

        #The zoom is constructed
        self.zoomGraphicsScene = QtWidgets.QGraphicsScene()
        self.zoomSize = 21
        self.zoomImageComboBox.setCurrentIndex(int((self.zoomSize - 11) / 2))
        self.zoomGraphicsView.setScene(self.zoomGraphicsScene)

        self.updateZoomSize()

        self.ctImage = QtGui.QImage(np.uint8(np.arange(255).reshape(1, 255)), 255, 1, 255, QtGui.QImage.Format_Indexed8)
        self.ctImage.setColorTable(self.createColorTable(self.defaultColorTable))
        self.ctPixmap = QtGui.QPixmap.fromImage(self.ctImage)
        self.colorTableLabel.setPixmap(self.ctPixmap)

        #self.actionQuit.setToolTip('You want to quit the program?')

        #Default path is defined
        self.path0 = os.path.realpath(os.path.curdir)
        self.path = self.path0
        #self.path = '/home/bepinat/Bureau/donnees_ghasp_saurons/results/calibration.ad3'

        self.scene = QtWidgets.QGraphicsScene()
        self.imItem = self.scene.addPixmap(QtGui.QPixmap())
        self.graphicsView.setScene(self.scene)
        self.graphicsView.scale(10, 10)  # XXX Wilfried?

        self.textEdit.clear()

        #Arguments associated to each image are defines
        self.argfiles = {'fullfilename': [], 'filename': [], 'data': [], 'numcube': [], 'sum': [], 'sumhighthr': [], 'sumlowthr': [], 'issum': [], 'profilenumber': [], 'header': [], 'sx': [], 'sy': [], 'sz': [], 'pixmap': [], 'zoompixmap': [], 'actionimage': [], 'actionprofile': [], 'max': [], 'min': [], 'colortable': [], 'highthr': [], 'lowthr': [], 'ctinvert': [], 'ctscale': [], 'zoom': [], 'flipx': [], 'flipy': [], 'mask': [], 'highmask': [], 'lowmask': [], 'formatval': [], 'thresholdstep': []}

        #Il faut initialiser les widgets d'affichage (data + header + zoom + profiles

        #Cursor shape definition
        #Le faire pour les différentes fenetres

    def savefile(self):
        """Popup to save the current file
        """
        #fname = QtWidgets.QFileDialog.getSaveFileName(self,  'Save file', QtCore.QString(self.path), "Images/Cubes (*.fits)") #python2.7
        #fname = str(fname.toUtf8()) #python2.7

        # If no global SAVEFILE is given, we open the dialog
        if SAVEFILE is None:
           fname = QtWidgets.QFileDialog.getSaveFileName(self,  'Save file', str(self.path), "Images/Cubes (*.fits)")
        else:
           # Path can be an empty string if given file is located in current folder
           if str(self.path) == '':
              path = './'
           elif str(self.path)[-1] != '/':
              path = str(self.path) + '/'
           else:
              path = str(self.path)
           fname = [path + SAVEFILE]

        try:
            header = self.argfiles['header'][self.numim - 1]
            data = self.argfiles['data'][self.numim - 1]
            hdu = fits.PrimaryHDU(data, header)
            hdulist = fits.HDUList(hdu)
            hdulist.writeto(fname[0], overwrite=True)
            print('Saved data into %s' %fname[0])
        except:
            print('Problem encountered when saving file')

    #def savefile(self):
        #"""Popup to save the current file
        #"""
        ##fname = QtWidgets.QFileDialog.getSaveFileName(self,  'Save file', QtCore.QString(self.path), "Images/Cubes (*.fits)") #python2.7
        ##fname = str(fname.toUtf8()) #python2.7
        #fname = QtWidgets.QFileDialog.getSaveFileName(self,  'Save file', str(self.path), "Images/Cubes (*.fits)")
        #try:
            #header = self.argfiles['header'][self.numim - 1]
            #data = self.argfiles['data'][self.numim - 1]
            #hdu = fits.PrimaryHDU(data, header)
            #hdulist = fits.HDUList(hdu)
            #hdulist.writeto(fname[0], overwrite=True)
        #except:
            #print('Problem encountered when saving file')


    def openfile_cl(self, fname):
        '''Open from command line call to PyQubeVis'''
        #Determination of extension
        if fname == '':
            return
        namesplit = os.path.splitext(fname)
        ext = namesplit[1].lower()
        if ext == '.gz':
            ext = os.path.splitext(namesplit[0])[1] + ext

        #File reading depending on extension
        if ext == '.fits':
            hdu = fits.open(fname)
        elif ext == '.fits.gz':
            hdu = fits.open(gzip.open(fname))
        elif ext == '.ad2':
            dtu = pad.read_ad2(fname)
            hdu = dtu.tohdu()
        elif ext == '.ad2.gz':
            dtu = pad.read_ad2(gzip.open(fname))
            hdu = dtu.tohdu()
        elif ext == '.ad3':
            dtu = pad.read_ad3(fname)
            hdu = dtu.tohdu()
        elif ext == '.ad3.gz':
            dtu = pad.read_ad3(gzip.open(fname))
            hdu = dtu.tohdu()
        else:
            return

        hdr = hdu[0].header
        data = hdu[0].data

        if self.numim == 0:
            self.displayScrollAreaWidgetContents.setEnabled(True)
        #Running path update
        #(self.path, filen) = os.path.split(fname)
        (self.path, filen) = os.path.split(fname)

        #Update of data arguments
        #self.argfiles['fullfilename'].append(fname)
        #self.colorTableList = QtColorTable()
        self.argfiles['fullfilename'].append(fname)
        self.argfiles['filename'].append(filen)
        self.argfiles['data'].append(np.array(data))
        self.argfiles['sum'].append(None)
        self.argfiles['sumhighthr'].append(None)
        self.argfiles['sumlowthr'].append(None)
        self.argfiles['issum'].append(0)
        self.argfiles['profilenumber'].append(1)
        self.argfiles['header'].append(hdr)
        if np.size(np.shape(data)) == 1:
            self.argfiles['sx'].append(np.shape(data)[0])
            self.argfiles['sy'].append(0)
            self.argfiles['sz'].append(0)
            self.argfiles['numcube'].append(0)
        if np.size(np.shape(data)) == 2:
            self.argfiles['sx'].append(np.shape(data)[1])
            self.argfiles['sy'].append(np.shape(data)[0])
            self.argfiles['sz'].append(0)
            self.argfiles['numcube'].append(0)
        if np.size(np.shape(data)) == 3:
            self.argfiles['sx'].append(np.shape(data)[2])
            self.argfiles['sy'].append(np.shape(data)[1])
            self.argfiles['sz'].append(np.shape(data)[0])
            self.numberofcubes += 1
            self.argfiles['numcube'].append(self.numberofcubes)

        #A mettre a jour correctement ou enlever?
        self.argfiles['pixmap'].append(None)
        self.argfiles['zoompixmap'].append(None)
        self.argfiles['actionimage'].append(None)
        self.argfiles['actionprofile'].append(None)
        self.argfiles['highthr'].append(np.nanmax(data))
        self.argfiles['lowthr'].append(np.nanmin(data))
        self.argfiles['ctinvert'].append(False)
        self.argfiles['ctscale'].append('Lin')
        self.argfiles['max'].append(np.nanmax(data))
        self.argfiles['min'].append(np.nanmin(data))
        self.argfiles['colortable'].append(self.createColorTable(self.defaultColorTable))
        self.argfiles['zoom'].append(1.)
        self.argfiles['flipx'].append(False)
        self.argfiles['flipy'].append(False)
        self.argfiles['mask'].append(None)
        self.argfiles['highmask'].append(None)
        self.argfiles['lowmask'].append(None)
        value_range = np.nanmax (data) - np.nanmin (data)
        if value_range == 0:
            formatval = 0
        else:
            formatval = np.ceil(- np.log10(value_range) + 4)
        self.argfiles['thresholdstep'].append(str(10 ** (-formatval + 3)))
        if formatval < 0: formatval = 0
        formatval = '%.' + '%if'%formatval
        self.argfiles['formatval'].append(formatval)

        self.numim = np.size(self.argfiles['filename'])
        if np.size(np.shape(data)) == 3:
            self.numcube = self.numim
        self.firstDisplay()


    def openfile(self):
        '''Popup to select the file to open'''
        #Il faudrait empecher de modifier le filtre
        #fname = QtWidgets.QFileDialog.getOpenFileName(self,  'Open file', QtCore.QString(self.path), "Images/Cubes (*.fits *.fits.gz *.ad2 *ad2.gz *.ad3 *ad3.gz)") #python2.7
        #fname = str(fname.toUtf8()) #python2.7
        fname = QtWidgets.QFileDialog.getOpenFileName(self,  'Open file', str(self.path), "Images/Cubes (*.fits *.fits.gz *.ad2 *ad2.gz *.ad3 *ad3.gz)")

        #Determination of extension
        if fname == '':
            return
        namesplit = os.path.splitext(fname[0])
        ext = namesplit[1].lower()
        if ext == '.gz':
            ext = os.path.splitext(namesplit[0])[1] + ext

        #File reading depending on extension
        if ext == '.fits':
            hdu = fits.open(fname[0])
        elif ext == '.fits.gz':
            hdu = fits.open(gzip.open(fname[0]))
        elif ext == '.ad2':
            dtu = pad.read_ad2(fname[0])
            hdu = dtu.tohdu()
        elif ext == '.ad2.gz':
            dtu = pad.read_ad2(gzip.open(fname[0]))
            hdu = dtu.tohdu()
        elif ext == '.ad3':
            dtu = pad.read_ad3(fname[0])
            hdu = dtu.tohdu()
        elif ext == '.ad3.gz':
            dtu = pad.read_ad3(gzip.open(fname[0]))
            hdu = dtu.tohdu()
        else:
            return

        hdr = hdu[0].header
        data = hdu[0].data

        if self.numim == 0:
            self.displayScrollAreaWidgetContents.setEnabled(True)
        #Running path update
        #(self.path, filen) = os.path.split(fname[0])
        (self.path, filen) = os.path.split(fname[0])

        #Update of data arguments
        #self.argfiles['fullfilename'].append(fname[0])
        #self.colorTableList = QtColorTable()
        self.argfiles['fullfilename'].append(fname[0])
        self.argfiles['filename'].append(filen)
        self.argfiles['data'].append(np.array(data))
        self.argfiles['sum'].append(None)
        self.argfiles['sumhighthr'].append(None)
        self.argfiles['sumlowthr'].append(None)
        self.argfiles['issum'].append(0)
        self.argfiles['profilenumber'].append(1)
        self.argfiles['header'].append(hdr)
        if np.size(np.shape(data)) == 1:
            self.argfiles['sx'].append(np.shape(data)[0])
            self.argfiles['sy'].append(0)
            self.argfiles['sz'].append(0)
            self.argfiles['numcube'].append(0)
        if np.size(np.shape(data)) == 2:
            self.argfiles['sx'].append(np.shape(data)[1])
            self.argfiles['sy'].append(np.shape(data)[0])
            self.argfiles['sz'].append(0)
            self.argfiles['numcube'].append(0)
        if np.size(np.shape(data)) == 3:
            self.argfiles['sx'].append(np.shape(data)[2])
            self.argfiles['sy'].append(np.shape(data)[1])
            self.argfiles['sz'].append(np.shape(data)[0])
            self.numberofcubes += 1
            self.argfiles['numcube'].append(self.numberofcubes)

        #A mettre a jour correctement ou enlever?
        self.argfiles['pixmap'].append(None)
        self.argfiles['zoompixmap'].append(None)
        self.argfiles['actionimage'].append(None)
        self.argfiles['actionprofile'].append(None)
        self.argfiles['highthr'].append(np.nanmax(data))
        self.argfiles['lowthr'].append(np.nanmin(data))
        self.argfiles['ctinvert'].append(False)
        self.argfiles['ctscale'].append('Lin')
        self.argfiles['max'].append(np.nanmax(data))
        self.argfiles['min'].append(np.nanmin(data))
        self.argfiles['colortable'].append(self.createColorTable(self.defaultColorTable))
        self.argfiles['zoom'].append(1.)
        self.argfiles['flipx'].append(False)
        self.argfiles['flipy'].append(False)
        self.argfiles['mask'].append(None)
        self.argfiles['highmask'].append(None)
        self.argfiles['lowmask'].append(None)
        value_range = np.nanmax (data) - np.nanmin (data)
        if value_range == 0:
            formatval = 0
        else:
            formatval = np.ceil(- np.log10(value_range) + 4)
        self.argfiles['thresholdstep'].append(str(10 ** (-formatval + 3)))
        if formatval < 0: formatval = 0
        formatval = '%.' + '%if'%formatval
        self.argfiles['formatval'].append(formatval)

        self.numim = np.size(self.argfiles['filename'])
        if np.size(np.shape(data)) == 3:
            self.numcube = self.numim
        self.firstDisplay()

    def firstDisplay(self):
        """
        """
        #Header display
        self.colorTableInvertCheckBox.setChecked(False)

        if self.argfiles['issum'][self.numim - 1]:
            self.sumCubeCheckBox.setCheckState(2)
        else:
            self.sumCubeCheckBox.setCheckState(0)

        self.textEdit.clear()
        self.textEdit.insertPlainText(repr(self.argfiles['header'][self.numim - 1]))

        #Image display
        if self.argfiles['sz'][self.numim - 1] == 0:
            self.im = self.argfiles['data'][self.numim - 1]
            self.channelSlider.setEnabled(False)
            self.channelLineEdit.setEnabled(False)
            self.sumCubeCheckBox.setEnabled(False)
        else:
            self.profileScrollAreaWidgetContents.setEnabled(True)
            self.channelSlider.setEnabled(True)
            self.channelLineEdit.setEnabled(True)
            self.sumCubeCheckBox.setEnabled(True)
            self.argfiles['sum'][self.numim - 1] = np.sum(self.argfiles['data'][self.numim - 1], axis=0)
            self.argfiles['sumlowthr'][self.numim - 1] = np.nanmin(self.argfiles['sum'][self.numim - 1])
            self.argfiles['sumhighthr'][self.numim - 1] = np.nanmax(self.argfiles['sum'][self.numim - 1])
            if self.sumCubeCheckBox.isChecked():
                self.im = self.argfiles['sum'][self.numim - 1]
            else:
                self.im = self.argfiles['data'][self.numim - 1][self.argfiles['profilenumber'][self.numim - 1] - 1, :, :]
                self.channelSlider.setMaximum(self.argfiles['sz'][self.numim - 1])
                self.channelSlider.setMinimum(1)
                self.channelSlider.setValue(self.argfiles['profilenumber'][self.numim - 1])
                self.channelLineEdit.setText(str(self.argfiles['profilenumber'][self.numim - 1]))
            self.line, = self.profileWidgetDisplay.canvas.ax.plot(self.argfiles['data'][self.numcube - 1][:, 0, 0], color='k')
            self.line.set_drawstyle('steps')
        #print np.shape(self.im), self.argfiles['sx'][self.numim - 1], self.argfiles['sy'][self.numim - 1], self.argfiles['sz'][self.numim - 1]

        self.graphicsView.setTransform(QtGui.QTransform(np.float32(1.), np.float32(0.), np.float32(0.), np.float32(-1.), np.float32(0.), np.float32(0.)), combine=False)
        self.zoomGraphicsView.setTransform(QtGui.QTransform(np.float32(1.), np.float32(0.), np.float32(0.), np.float32(-1.), np.float32(0.), np.float32(0.)), combine=False)
        #self.graphicsView.setTransform(QtGui.QTransform(np.float32(1.), np.float32(0.), np.float32(0.), np.float32(1.), np.float32(0.), np.float32(0.)), combine=False)
        #self.zoomGraphicsView.setTransform(QtGui.QTransform(np.float32(1.), np.float32(0.), np.float32(0.), np.float32(1.), np.float32(0.), np.float32(0.)), combine=False)
        self.zoomGraphicsView.scale(float(self.zoomGraphicsView.width()) / self.zoomSize, float(self.zoomGraphicsView.height()) / self.zoomSize)
        self.flipxCheckBox.setChecked(False)
        self.flipyCheckBox.setChecked(False)

        self.updateColorTable()

        self.graphicsView.centerOn(self.imItem)
        fac = 2 ** np.floor(np.log(np.min((float(self.graphicsView.height()), float(self.graphicsView.width())) / np.max(self.im.shape))) / np.log(2.))
        self.argfiles['zoom'][self.numim - 1] = fac
        self.graphicsView.scale(fac, fac)


        # WARNING: ADDED TO BETTER SEE
        #self.graphicsView.scale(4, 4)  # XXX Wilfried?
        
        #self.scaleComboBox.setCurrentIndex(self.scaleComboBox.findText(QtCore.QString(self.argfiles['ctscale'][self.numim - 1]))) #python2.7
        self.scaleComboBox.setCurrentIndex(self.scaleComboBox.findText(str(self.argfiles['ctscale'][self.numim - 1])))

        #Action is displayed and saved
        self.argfiles['actionimage'][self.numim - 1] = self.menuImages.addAction(str(self.numim) + ' - ' + self.argfiles['filename'][self.numim - 1])
        self.argfiles['actionimage'][self.numim - 1].triggered.connect(self.recovernumim)
        if self.argfiles['sz'][self.numim - 1] != 0:
            self.argfiles['actionprofile'][self.numim - 1] = self.menuProfiles.addAction(str(self.argfiles['numcube'][self.numim - 1]) + ' - ' + self.argfiles['filename'][self.numim - 1])
            self.argfiles['actionprofile'][self.numim - 1].triggered.connect(self.recovernumcube)

        #mettre a jour le menu images, profiles les Qlabels size X, size Y
        self.xsizeLabel.setText('X = %i'%(self.argfiles['sx'][self.numim - 1]))
        self.ysizeLabel.setText('Y = %i'%(self.argfiles['sy'][self.numim - 1]))
        if self.argfiles['sz'][self.numim - 1] > 1:
            self.zsizeLabel.setText('Z = %i'%(self.argfiles['sz'][self.numim - 1]))
        else:
            self.zsizeLabel.setText('Z = ')
        self.minLabel.setText('Min = ' + self.argfiles['formatval'][self.numim - 1]%(self.argfiles['min'][self.numim - 1]))
        self.maxLabel.setText('Max = ' + self.argfiles['formatval'][self.numim - 1]%(self.argfiles['max'][self.numim - 1]))
        
        if self.sumCubeCheckBox.isChecked():
            self.highThresholdLineEdit.setText(self.argfiles['formatval'][self.numim - 1]%(self.argfiles['sumhighthr'][self.numim - 1]))
            self.lowThresholdLineEdit.setText(self.argfiles['formatval'][self.numim - 1]%(self.argfiles['sumlowthr'][self.numim - 1]))
        else:
            self.highThresholdLineEdit.setText(self.argfiles['formatval'][self.numim - 1]%(self.argfiles['highthr'][self.numim - 1]))
            self.lowThresholdLineEdit.setText(self.argfiles['formatval'][self.numim - 1]%(self.argfiles['lowthr'][self.numim - 1]))
        self.thresholdComboBox.setEditText(self.argfiles['thresholdstep'][self.numim - 1])

    def nextDisplay(self):
        """
        """
        #Header display
        self.textEdit.clear()
        self.textEdit.insertPlainText(repr(self.argfiles['header'][self.numim - 1]))

        self.colorTableInvertCheckBox.setChecked(self.argfiles['ctinvert'][self.numim - 1])

        if self.argfiles['issum'][self.numim - 1]:
            self.sumCubeCheckBox.setCheckState(2)
        else:
            self.sumCubeCheckBox.setCheckState(0)

        #Image display
        if self.argfiles['sz'][self.numim - 1] == 0:
            self.im = self.argfiles['data'][self.numim - 1]
            self.channelSlider.setEnabled(False)
            self.channelLineEdit.setEnabled(False)
            self.sumCubeCheckBox.setEnabled(False)
        else:
            self.channelSlider.setEnabled(True)
            self.channelLineEdit.setEnabled(True)
            self.sumCubeCheckBox.setEnabled(True)
            if self.sumCubeCheckBox.isChecked():
                self.im = np.sum(self.argfiles['data'][self.numim - 1], axis=0)
            else:
                self.im = self.argfiles['data'][self.numim - 1][self.argfiles['profilenumber'][self.numim - 1] - 1, :, :]
                self.channelSlider.setMaximum(self.argfiles['sz'][self.numim - 1])
                self.channelSlider.setMinimum(1)
                self.channelSlider.setValue(self.argfiles['profilenumber'][self.numim - 1])
                self.channelLineEdit.setText(str(self.argfiles['profilenumber'][self.numim - 1]))
            self.line, = self.profileWidgetDisplay.canvas.ax.plot(self.argfiles['data'][self.numcube - 1][:, 0, 0], color='k')
            self.line.set_drawstyle('steps')

        self.updateColorTable()
        #self.scaleComboBox.setCurrentIndex(self.scaleComboBox.findText(QtCore.QString(self.argfiles['ctscale'][self.numim - 1]))) #python2.7
        self.scaleComboBox.setCurrentIndex(self.scaleComboBox.findText(str(self.argfiles['ctscale'][self.numim - 1])))
        #-----------------#
        #Scene update

        #Zoom recovery
        facprev = self.argfiles['zoom'][self.numimprev - 1]
        fac = self.argfiles['zoom'][self.numim - 1]
        self.graphicsView.scale(fac / facprev, fac / facprev)
        self.imItem.setPixmap(self.argfiles['pixmap'][self.numim - 1])

        #Flip recovery
        flipx = self.argfiles['flipx'][self.numim - 1]
        flipy = self.argfiles['flipy'][self.numim - 1]
        if self.argfiles['flipx'][self.numim - 1] != self.argfiles['flipx'][self.numimprev - 1]:
            self.argfiles['flipx'][self.numim - 1] = self.argfiles['flipx'][self.numimprev - 1]
            self.flipxCheckBox.setChecked(flipx)
        if self.argfiles['flipy'][self.numim - 1] != self.argfiles['flipy'][self.numimprev - 1]:
            self.argfiles['flipy'][self.numim - 1] = self.argfiles['flipy'][self.numimprev - 1]
            self.flipyCheckBox.setChecked(flipy)

        #Image information update
        self.xsizeLabel.setText('X = %i'%(self.argfiles['sx'][self.numim - 1]))
        self.ysizeLabel.setText('Y = %i'%(self.argfiles['sy'][self.numim - 1]))
        self.minLabel.setText('Min = ' + self.argfiles['formatval'][self.numim - 1]%(self.argfiles['min'][self.numim - 1]))
        self.maxLabel.setText('Max = ' + self.argfiles['formatval'][self.numim - 1]%(self.argfiles['max'][self.numim - 1]))
        
        if self.argfiles['sz'][self.numim - 1] > 1:
            self.zsizeLabel.setText('Z = %i'%(self.argfiles['sz'][self.numim - 1]))
        else:
            self.zsizeLabel.setText('Z = ')

        if self.sumCubeCheckBox.isChecked():
            self.highThresholdLineEdit.setText(self.argfiles['formatval'][self.numim - 1]%(self.argfiles['sumhighthr'][self.numim - 1]))
            self.lowThresholdLineEdit.setText(self.argfiles['formatval'][self.numim - 1]%(self.argfiles['sumlowthr'][self.numim - 1]))
        else:
            self.highThresholdLineEdit.setText(self.argfiles['formatval'][self.numim - 1]%(self.argfiles['highthr'][self.numim - 1]))
            self.lowThresholdLineEdit.setText(self.argfiles['formatval'][self.numim - 1]%(self.argfiles['lowthr'][self.numim - 1]))
        self.thresholdComboBox.setEditText(self.argfiles['thresholdstep'][self.numim - 1])

    def recovernumim(self):
        sender = self.sender()
        if self.numim == np.int8(str(sender.text()).split('-')[0]):
            return
        self.numimprev = self.numim
        self.numim = np.int8(str(sender.text()).split('-')[0])
        self.nextDisplay()

    def recovernumcube(self):
        sender = self.sender()
        index = np.int8(str(sender.text()).split('-')[0])
        if self.numcube == int(np.where(self.argfiles['numcube'] == index)[0]) + 1:
            return
        self.numcube = int(np.where(self.argfiles['numcube'] == index)[0]) + 1

    def about(self):
        '''Popup a box with about message.'''
        QMessageBox.about(self, "About PyQubeVis",
                          """<b>PyQubeVis </b> v %s
                          <p>Program by Benoit Epinat.
                          All rights reserved in accordance with
                          GPL v3 or later - NO WARRANTIES!
                          <p>This application can be used to display fits and adhocw format images or cubes.
                          <p>Python %s - PyQt5 version %s - Qt version %s""" % (__version__,
                          platform.python_version(), PYQT_VERSION_STR, QT_VERSION_STR))


def main(argv):
    global SAVEFILE
    
    app = QApplication(sys.argv)
    #parser = argparse.ArgumentParser()
    #parser.add_argument('filename', action="store", default='', help="name of the file to open")
    #options = parser.parse_args(app.arguments()[1:])
    #print(options.filename, len(options.filename))
    
    # Set fullscreen if required
    if '-f' in sys.argv[1:]:
      fullscreen = True
      sys.argv.remove('-f')
    else:
      fullscreen = False

    # Set save file if required
    if '-s' in sys.argv[1:]:
      SAVEFILE = str(sys.argv[1:][sys.argv[1:].index('-s')+1])
      sys.argv.remove('-s')
      sys.argv.remove(SAVEFILE)
      print('Cleaned file shall be saved as %s' %SAVEFILE)
    
    frame = MainWindow()
    for fname in sys.argv[1:]:
        frame.openfile_cl(fname)
    frame.show()

    if fullscreen:
       frame.showMaximized()
    sys.exit(app.exec_())

if __name__ == '__main__':
    SAVEFILE = None
    main(sys.argv[1:])

